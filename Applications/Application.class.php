<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Library\Applications;

use \Library\Factory;
use \Library\Interfaces\IApplication;
use \Library\Interfaces\ICache;
use \Library\Interfaces\IConfig;
use \Library\Interfaces\iErrorManager;
use \Library\Interfaces\iLanguage;
use \Library\Interfaces\IUser;

/**
 * Description of Application
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
abstract class Application implements IApplication
{
    private $name = '';
    protected $cache = null;
    protected $config = null;
    protected $factory = null;
    protected $errorManager = null;
    protected $user = null;
    protected $language = null;

    public function __construct()
    {
        $this->initFactory();
        $this->init();
        $this->cache = $this->factory->get('Library/Interfaces/ICache');
        $this->config = $this->factory->get('Library/Interfaces/IConfig');
        $this->errorManager = $this->factory->get('Library/Interfaces/ILogger');
        $this->user = $this->factory->get('Library/Interfaces/IUser');
        $this->language = $this->factory->get('Library/Language');
    }

    protected function initFactory()
    {
        $this->setFactory(new Factory);
    }

    abstract protected function init();

    abstract public function run();

    /**
     * @return cache
     */
    public function cache()
    {
        return $this->cache;
    }

    /**
     * @param $cache
     * @return self
     */
    public function setCache(ICache $cache)
    {
        $this->cache = $cache;
        return $this;
    }

    /**
     * @return config
     */
    public function config()
    {
        return $this->config;
    }

    /**
     * @param $config
     * @return self
     */
    public function setConfig(Config $config)
    {
        $this->config = $config;
        return $this;
    }

    /**
     * @return errorManager
     */
    public function errorManager()
    {
        return $this->errorManager;
    }

    /**
     * @param $errorManager
     * @return self
     */
    public function setErrorManager(iErrorManager $errorManager)
    {
        $this->errorManager = $errorManager;
        return $this;
    }

    /**
     * @return factory
     */
    public function factory()
    {
        return $this->factory;
    }

    /**
     * @param $factory
     * @return self
     */
    public function setFactory(Factory $factory)
    {
        $this->factory = $factory;
        return $this;
    }

    /**
     * @return language
     */
    public function language()
    {
        return $this->language;
    }

    /**
     * @param $language
     * @return self
     */
    public function setLanguage(Language $language)
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @return name
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * @param $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return user
     */
    public function user()
    {
        return $this->user;
    }

    /**
     * @param $user
     * @return self
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }
}
