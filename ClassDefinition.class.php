<?php
namespace Library;

use \Library\ClassDependency;
use \Library\ClassParameter;

class ClassDefinition extends ClassDependency
{
    private $parentClassName = '';
    private $parameters = array();
    private $dependencies = array();

    /**
     * @return parentClassName
     */
    public function parentClassName()
    {
        return $this->parentClassName;
    }

    /**
     * @param $parentClassName
     * @return self
     */
    public function setParentClassName($parentClassName)
    {
        $this->parentClassName = $parentClassName;
        return $this;
    }

    /**
     * @return parameters
     */
    public function parameters()
    {
        return $this->parameters;
    }

    /**
     * @param $parameters
     * @return self
     */
    public function setClassParameters(array $parameters)
    {
        foreach ($parameters as $parameter) {
            $this->addClassParameter($parameter);
        }
        return $this;
    }

    public function addClassParameter(ClassParameter $parameter)
    {
        $this->parameters[] = $parameter;
        return $this;
    }

    /**
     * @return dependencies
     */
    public function dependencies()
    {
        return $this->dependencies;
    }

    /**
     * @param $dependencies
     * @return self
     */
    public function setClassDependencies(array $dependencies)
    {
        foreach ($dependencies as $dependency) {
            $this->addClassDependency($dependency);
        }
        return $this;
    }

    public function addClassDependency(ClassDependency $dependency)
    {
        $this->dependencies[] = $dependency;
        return $this;
    }
}
