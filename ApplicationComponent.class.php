<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Library;

use \Library\Interfaces\IApplication;

/**
 * Description of ApplicationComponent
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
abstract class ApplicationComponent
{
    protected $app = null;

    public function __construct(IApplication $application)
    {
        $this->app = $application;
    }
}
