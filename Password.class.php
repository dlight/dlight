<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
namespace Library;

/**
 * Wrapper object for external library password_compat.
 * This library is intended to provide forward compatibility with the password_*
 * functions being worked on for PHP 5.5.
 * @source https://github.com/ircmaxell/password_compat
 * @todo remove password_compat dependency on systems using PHP5.5+
 *
 * Salt is generated with bin2hex(openssl_random_pseudo_bytes(22)),
 * which creates a BCRYPT BASE64 string of 44 chars:
 * @source https://gist.github.com/dzuelke/972386
 */
class Password extends ApplicationComponent
{
    public function __construct(IApplication $app)
    {
        parent::__construct($app);
        require 'External/password_compat/lib/password.php';
    }

    public function hash($password)
    {
        //@todo can return false
        return password_hash($password, PASSWORD_BCRYPT);
    }

    public function verify($password, $hash)
    {
        if (!($isMatch = password_verify($password, $hash))) {
            $this->app->errorManager()->logMessage(__METHOD__.':'.__LINE__.': entered password does not match '.$hash);
        }
        return $isMatch;
    }
}
