<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Library\FileManagers;

use \Library\FileManager;
use \Library\Interfaces\IXmlFileManager;
use \SimpleXMLElement;

class XmlFileManager extends FileManager implements IXmlFileManager
{
    private $root = null;

    public function root($tagName = 'root')
    {
        if (!$this->root) {
            $this->initRoot($tagName);
        }
        return $this->root;
    }

    public function openXmlFile($filename)
    {
        if (file_exists($filename)) {
            $this->logger->logMessage(__METHOD__.':'.__LINE__.": loading $filename.");
            $this->root = simplexml_load_file($filename);
        } else {
            $this->errorEncountered = true;
            $this->logger->logError(__METHOD__.':'.__LINE__.": $filename does not exist.");
        }
        return $this->root();
    }

    public function initRoot($tagName)
    {
        $this->root = new SimpleXMLElement("<$tagName></$tagName>");
        return $this;
    }

    public function setRoot(SimpleXMLElement $root)
    {
        $this->root = $root;
        return $this;
    }

    public function addChild($childName)
    {
        return $this->root->addChild($childName);
    }

    public function saveXmlFile($pathFolder, $filename, $saveEmptyRoot = false)
    {
        $isFileSaved = FALSE;
        if (
            ($saveEmptyRoot || $this->root->count()) &&
            $this->createFolder($pathFolder)
        ) {
            if (($this->errorEncountered = $this->root->asXML("$pathFolder/$filename"))) {
                $isFileSaved = TRUE;
                $this->logger->logMessage(__METHOD__.__LINE__.": $pathFolder/$filename has been created");
            } else {
                $this->logger->logMessage(__METHOD__.__LINE__." could not create $pathFolder/$filename");
            }
        }
        return $isFileSaved;
    }

    /**
     * Update $childNodeName if it exists in $node, or create it.
     * @param SimpleXMLElement $node The parent node.
     * @param string $childNodeName The node to update.
     * @param string $newValue The new value.
     */
    public function update(SimpleXMLElement $node, $childNodeName, $newValue)
    {
        // Don't compare to null because a SimpleXMLElement is always returned,
        // even if the node does not exist.
        if (empty($node->$childNodeName)) {
            $node->addChild($childNodeName, (string)$newValue);
        } else {
            $node->$childNodeName = (string)$newValue;
        }
    }

    public function parseXmlToObjects($filename, $attributeAsKey = '')
    {
        $builtObjects = array();
        $builtObject = null;
        $this->openXmlFile($filename);
        //@todo read namespace from nodeName by using allowed special char by convention -_ ?
        foreach ($this->root() as $nodeName => $xmlNode) {
            $builtObject = $this->buildObject($nodeName, $xmlNode);
            foreach ($xmlNode->children() as $childName => $childNode) {
                $childObject = $this->buildObject($childName, $childNode);
                $setterTemplate = '%s'.ucfirst($childName);
                $setter = sprintf($setterTemplate, 'add');
                if (!method_exists($builtObject, $setter)) {
                    $setter = sprintf($setterTemplate, 'set');
                }
                $builtObject->$setter($childObject);
            }
            if ($attributeAsKey && ($key = $this->getAttribute($xmlNode, $attributeAsKey))) {
                $builtObjects[$key] = $builtObject;
            } else {
                $builtObjects[] = $builtObject;
            }
        }
        return $builtObjects;
    }

    private function buildObject($nodeName, $xmlNode)
    {
        $objectNamespace = $this->config()->getString('defaultNamespace');
        $className = "$objectNamespace\\$nodeName";
        $builtObject = new $className;
        foreach ($xmlNode->attributes() as $attributeName => $attributeValue) {
            $setter = 'set'.ucfirst($attributeName);
            // Attribute value is a SimpleXMLElement and must be converted to string.
            $builtObject->$setter((string)$attributeValue);
        }
        return $builtObject;
    }

    private function getAttribute($xmlNode, $attributeName)
    {
        return (string)$xmlNode->attributes()->$attributeName;
    }

    private function getFirstAttribute($xmlNode)
    {
        $firstAttribute = '';
        $attributes = $xmlNode->attributes();
        if (count($attributes) > 0) {
            $firstAttribute = (string)array_shift($attributes);
        }
        return $firstAttribute;
    }

    public function parseXmlToKeyValueArray($filename, $attributeAsKey = 'key', $attributeAsValue = 'value')
    {
        $values = array();
        $this->openXmlFile($filename);
        foreach ($this->root() as $nodeName => $xmlNode) {
            $key = (string)$xmlNode->attributes()[$attributeAsKey];
            $value = (string)$xmlNode->attributes()[$attributeAsValue];
            $values[$key] = $value;
        }
        return $values;
    }

    /**
     * Adding a complete node to another node is not supported by the SimpleXML API.
     * This method use DOM to do it.
     * @source http://stackoverflow.com/a/4778964
     */
    public function appendChild(SimpleXMLElement $parent, SimpleXMLElement $child)
    {
        $parentDom = dom_import_simplexml($parent);
        $childDom = dom_import_simplexml($child);
        $parentDom->appendChild($parentDom->ownerDocument->importNode($childDom, true));
    }

    public function deleteFiles(array $filenames)
    {
        $this->setFileSuffix('xml');
        parent::deleteFiles($filenames);
    }
}
