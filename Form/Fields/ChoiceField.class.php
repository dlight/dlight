<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Library\Form\Fields;

use \Library\Form\Field;
use \Library\Form\Fields\StringField;

/**
 * Description of ChoiceField
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
class ChoiceField extends Field
{
    protected $choices = array();
    protected $isMultiple = false;

    public function buildWidget()
    {
        $widget = '';
        $selectClass = '';
        if ($this->isMultiple) {
            $stringWidget = (new StringField(array(
                'name' => $this->id().'_display',
                'readonly' => true,
            )))->setCreateLabelOff();
            if ($this->isMandatory()) {
                $stringWidget->setMandatory();
            }
            // @todo use property to remove css dependency
            if ($this->hasClass('hidden')) {
                $stringWidget->addClass('hidden');
            }
            $widget .= $stringWidget;
            $selectClass = ' hidden';
        }
        $widget .= '<select name="'.$this->name;
        if ($this->isMultiple) {
            $widget .= '[]" multiple="multiple"';
        } else {
            $widget .= '"';
        }
        $widget .= ' id="'.$this->id().'" size="0" class="'.$this->classesString().$selectClass.'"';
        if ($this->readonly) {
            $widget .= ' disabled="disabled"';
        }
        $widget .= '>';
        foreach ($this->choices as $option_value => $option_text) {
            $text = '';
            $isActive = true;
            if (is_array($option_text)) {
                $text = (string)array_shift($option_text);
                $isActive = (bool)array_shift($option_text);
            } else {
                $text = $option_text;
            }
            /*
             * Bad idea because if no default option, user cannot empties a previous selection.
             * BAD: if multiple choices are allowed, does not create a default value.
             */
//            if( !($this->isMultiple && $option_value == 0)) {
            $widget .= '<option value="'.$option_value.'"';
            if (
                (!$this->isMultiple && $option_value == $this->value()) ||
                (is_array($this->value()) && $this->isMultiple && array_search($option_value, $this->value()) !== false)
            ) {
                $widget .= ' selected="selected"';
            }
            // UGLY @todo app specific, move to view
            if ($text == 'job offer') {
                $widget .= ' class="optionSecondary"';
            }
            if (!$isActive) {
                $widget .= ' disabled="disabled"';
            }
            $widget .= '>'.$text.'</option>';
        }
        $this->finalRender = $widget .= '</select>';
    }

    public function setChoices(array $choices)
    {
        $this->choices = $choices;
    }

    public function setIsMultiple($isMultiple)
    {
        if (is_bool($isMultiple)) {
            $this->isMultiple = $isMultiple;
        }
    }
}
