<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Library\Form\Fields;

use \Library\Entity;
use \Library\Form\Field;
use \Library\Form\Fields\SingleCheckboxField;
use \Library\Form\Fields\ChoiceField;
use \Library\Form\Fields\StringField;

/**
 * Description of MobilityField
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
class CheckboxActivatedField extends Field
{
    private $isChecked = false;
    private $fieldValues = array();
    private $createEmailField = false;
    protected $choices = array();
    protected $isMultiple = false;

    public function __construct(array $options = array())
    {
        parent::__construct($options);
        //$this->setContainerClass('checkbox_with_label');
    }

    public function setIsChecked($isChecked)
    {
        $this->isChecked = $isChecked;
    }

    public function setFieldValues(array $fieldValues)
    {
        $this->fieldValues = $fieldValues;
    }

    public function setValue($value)
    {
        if (is_array($value)) {
            $this->value = array(
                'isChecked' => $this->isChecked,
                'fieldValue' => $this->fieldValues,
            );
        } else {
            parent::setValue($value);
        }
    }

    public function buildWidget()
    {
        $isChecked = $this->isChecked;
        $fieldValues = $this->value;
        // input checkbox only returns its value if it has been checked,
        // but input text always returns a value (empty if the field is empty)
        // so I must check if isChecked exists
        if (is_array($this->value) && !empty($this->value) && isset($this->value['isChecked'])) {
            $isChecked = $this->value['isChecked'];
            $fieldValues = $this->value['fieldValue'];
        }
        $checkboxField = new SingleCheckboxField(array(
            'label' => $this->label()->text(),
            'name' => $this->name().'[isChecked]',
            'id' => $this->name().'_isChecked',
        ));
        $checkboxField->addClass('checkbox_classic checkbox_alone inline displaySelectBox');
        if ($this->isMandatory()) {
            $checkboxField->setMandatoryDisplayOnly();
            $checkboxField->setMandatory();
        }
        $options = array(
            'name' => $this->name().'[fieldValue]',
            'id' => $this->name().'_fieldValue',
            'value' => $fieldValues,
        );
        // if choices are set, create select, input text otherwise
        if (!empty($this->choices)) {
            $options['choices'] = $this->choices;
            $options['isMultiple'] = $this->isMultiple;
            $field = new ChoiceField($options);
        } else {
            if ($this->createEmailField) {
                $field = new EmailField($options);
            } else {
                $field = new StringField($options);
            }
        }
        if ($isChecked) {
            $checkboxField->setChecked();
        } else {
            $field->addClass('hidden');
        }
        // @todo remove css dependency
        $this->finalRender .= $checkboxField.' '.$field->setCreateLabelOff()->addClass('checkboxActivatedField');
    }

    public function setChoices(array $choices)
    {
        $this->choices = $choices;
    }

    public function setIsMultiple($isMultiple)
    {
        if (is_bool($isMultiple)) {
            $this->isMultiple = $isMultiple;
        }
    }

    public function setCreateEmailField()
    {
        $this->createEmailField = true;
        return $this;
    }
}
