<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
namespace Library\Form\Fields;

use \Library\Form\Field;
use \Library\Form\Fields\SingleCheckboxField;
use \Library\Utils\ArrayUtils;

/**
 * Description of ChoiceCheckboxField
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
class ChoiceCheckboxField extends Field
{
    protected $choices = array();
    protected $isChoicesSimpleArray = false;
    protected $keyForSorting = array();
    protected $keyForValue = null;
    protected $keyForText = '';

    public function widgets()
    {
        $widgetsList = array();
        foreach ($this->choices as $key => $choice) {
            if ($this->keyForValue === null) {
                $this->keyForValue = $key;
            }
            if ($this->keyForText == '' && is_string($choice)) {
                $this->keyForText = $choice;
            }
            if (is_array($choice)) {
                $value = $choice[$this->keyForValue];
                $text = $choice[$this->keyForText];
            } elseif (is_string($choice)) {
                $value = $key;
                $text = $choice;
            }
            $id = $this->id().$value;
            $widget = (new SingleCheckboxField)->setId($id)->setNamePrefix($this->namePrefix())
                ->setName($this->name().'[]')->setValue($value)->addClass($this->fieldClassesString())->setLabel($text);
            if (array_search($value, $this->value()) !== false) {
                $widget->setChecked();
            }
            if (!$this->isChoicesSimpleArray) {
                $keyForSorting = '';
                foreach ($this->keyForSorting as $subvalue) {
                    $keyForSorting .= $choice[$subvalue].'-';
                }
                $keyForSorting = substr($keyForSorting, 0, -1);
                if (!isset($widgetsList[$keyForSorting])) {
                    $widgetsList[$keyForSorting] = array();
                }
                $widgetsList[$keyForSorting][$value] = $widget;
            } else {
                array_push($widgetsList, $widget);
            }
        }
        return $widgetsList;
        //$this->finalRender = (new ArrayUtils)->recursiveImplode(' ', $widgetsList);
    }

    public function buildWidget()
    {
        $this->finalRender = '';
    }

    public function setChoices($choices)
    {
        if (is_array($choices)) {
            $this->choices = $choices;
        }
    }

    public function setIsChoicesSimpleArray($isChoicesSimpleArray)
    {
        $this->isChoicesSimpleArray = $isChoicesSimpleArray;
    }

    public function setKeyForSorting($keyForSorting)
    {
        if (!$this->isChoicesSimpleArray && is_array($keyForSorting)) {
            $this->keyForSorting = $keyForSorting;
        }
    }

    public function setKeyForValue($keyForValue)
    {
        if (!$this->isChoicesSimpleArray && is_string($keyForValue) && !empty($keyForValue)) {
            $this->keyForValue = $keyForValue;
        }
    }

    public function setKeyForText($keyForText)
    {
        if (!$this->isChoicesSimpleArray && is_string($keyForText) && !empty($keyForText)) {
            $this->keyForText = $keyForText;
        }
    }
}
