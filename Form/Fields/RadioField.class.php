<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
namespace Library\Form\Fields;

use \Library\Form\Field;

/**
 * Description of RadioField
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
class RadioField extends Field
{
    protected $choices = array();
    protected $switchRadio = false;

    public function buildWidget()
    {
        $ulClass = '';
        if ($this->switchRadio) {
            $ulClass = 'switch_radio';
        }
        $widget = '
            <ul class="'.$ulClass.'">';
        foreach ($this->choices as $id => $label) {
            $widget .= '<li class="';
            $checkedString = '';
            if ($id == $this->value()) {
                $checkedString = ' checked="checked"';
            } elseif ($this->switchRadio) {
                $widget .= 'hidden';
            }
            $widget .= '">
                    <input id="'.$this->id().'_'.$id.'" type="radio" name="'.
                        $this->name.'" value="'.$id.'"'.$checkedString.'/>
                    <label for="'.$this->id().'_'.$id.'">'.$label.'</label>
                </li>';
        }
        $widget .= '</ul>';
        $this->finalRender = $widget;
    }

    public function choices()
    {
        return $this->choices;
    }

    public function setChoices(array $choices)
    {
        $this->choices = $choices;
    }

    public function setSwitchRadio()
    {
        $this->switchRadio = true;
        return $this;
    }
}
