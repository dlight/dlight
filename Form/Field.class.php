<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
namespace Library\Form;

use \Exception;
use \Library\DomElements\DomElement;
use \Library\Form\Label;

/**
 * Description of Field
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
abstract class Field extends DomElement
{
    protected $errorMessage = '';
    protected $fieldClasses = array();
    protected $createLabel = true;
    private $labelBefore = true;
    protected $label = null;
    protected $namePrefix = '';
    protected $name = '';
    private $nameNoPrefix = '';
    protected $value = null;
    protected $isMandatory = false;
    private $mandatoryDisplayOnly = false;
    protected $validators = array();
    protected $readonly = false;

    public function __construct(array $options = array())
    {
        if (!empty($options)) {
            $this->hydrate($options);
        }
    }

    public function buildErrors()
    {
        $widget = '';
        if (!empty($this->errorMessage)) {
            $widget = '<p class="form_error_message text_info">'.$this->errorMessage.'</p>';
        }
        return $widget;
    }

    public function hydrate($options)
    {
        foreach ($options as $type => $value) {
            $method = 'set'.ucfirst($type);
            if (is_callable(array($this, $method))) {
                $this->$method($value);
            }
        }
    }

    public function isValid()
    {
        foreach ($this->validators as $validator) {
            if (!$validator->isValid($this->value)) {
//                $this->errorMessage .= $validator->errorMessage();
                return false;
            }
        }
        return true;
    }

    /**
     * @return errorMessage
     */
    public function errorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @param $errorMessage
     * @return self
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;
        return $this;
    }

    /**
     * @return fieldClasses
     */
    public function fieldClasses()
    {
        return $this->fieldClasses;
    }

    /**
     * @param $fieldClasses
     * @return self
     */
    public function setFieldClasses(array $fieldClasses)
    {
        foreach ($fieldClasses as $fieldClass) {
            $this->addFieldClass($fieldClass);
        }
        return $this;
    }

    public function fieldClassesString()
    {
        return implode(' ', $this->fieldClasses());
    }

    public function addFieldClass($fieldClass)
    {
        $this->fieldClasses[] = $fieldClass;
    }

    /**
     * @return createLabel
     */
    public function createLabel()
    {
        return $this->createLabel;
    }

    public function setCreateLabelOff()
    {
        $this->createLabel = false;
        return $this;
    }

    /**
     * @return label
     */
    public function label()
    {
        return $this->label;
    }

    /**
     * @param $label
     * @return self
     */
    public function setLabel($label)
    {
        $this->label = new Label($this, $label);
        return $this;
    }

    /**
     * @return namePrefix
     */
    public function namePrefix()
    {
        return $this->namePrefix;
    }

    /**
     * @param $namePrefix
     * @return self
     */
    public function setNamePrefix($namePrefix)
    {
        $this->namePrefix = $namePrefix;
        return $this;
    }

    /**
     * @return name
     */
    public function fullname()
    {
        return $this->namePrefix().$this->name;
    }

    public function name()
    {
        return $this->name;
    }

    /**
     * @param $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        $this->addAttribute('name', $name);
        $id = $this->id();
        if (empty($id)) {
            $this->setId($this->fullname());
        }
        $label = $this->label;
        if (empty($label)) {
            $this->setLabel(strtoupper(substr($name, 0, 1)).substr($name, 1));
        }
        return $this;
    }

    /**
     * @return value
     */
    public function value()
    {
        return $this->value;
    }

    /**
     * @param $value
     * @return self
     */
    public function setValue($value)
    {
        $this->value = $value;
        if (gettype($value) == 'string') {
            $this->setStringValue();
        }
        $this->addAttribute('value', $this->value);
        return $this;
    }

    protected function setStringValue()
    {
        $this->value = htmlspecialchars(stripslashes($this->value));
    }

    /**
     * @return isMandatory
     */
    public function isMandatory()
    {
        return $this->isMandatory;
    }

    /**
     * @deprecated
     * @param bool $isMandatory
     * @return self
     */
    public function setIsMandatory($isMandatory)
    {
        return $this->setMandatory();
    }

    public function setMandatory()
    {
        $this->isMandatory = true;
        if (!$this->mandatoryDisplayOnly) {
            $this->addAttribute('required', 'required');
        }
        return $this;
    }

    protected function setMandatoryOff()
    {
        $this->isMandatory = false;
        $this->removeAttribute('required');
    }

    public function setMandatoryDisplayOnly()
    {
        $this->mandatoryDisplayOnly = true;
        $this->removeAttribute('required');
    }

    /**
     * @return validators
     */
    public function validators()
    {
        return $this->validators;
    }

    /**
     * @param $validators
     * @return self
     */
    public function setValidators(array $validators)
    {
        foreach ($validators as $validator) {
            $this->addValidator($validator);
        }
        return $this;
    }

    public function addValidator(Validator $validator)
    {
        $this->validators[] = $validator;
    }

    public function readonly()
    {
        return $this->readonly;
    }

    public function setReadonly()
    {
        $this->readonly = true;
        return $this;
    }

    public function __tostring()
    {
        $finalString = '';
        try {
            $this->buildWidget();
            $label = '';
            $finalRender = '';
            if ($this->createLabel()) {
                $this->label()->buildWidget();
                $label = $this->label()->finalRender;
            }
            if ($this->labelBefore) {
                $finalRender = $label.$this->finalRender;
            } else {
                $finalRender = $this->finalRender.$label;
            }
            $finalString = $finalRender;
        } catch (Exception $exception) {
            if (stripos('127.0.0.1', $_SERVER['REMOTE_ADDR']) !== false && $_SERVER['SERVER_ADDR'] == '127.0.0.1') {
                var_dump($exception);
            }
            $finalString = 'An error preventing this content to appear properly occured.'.
               ' Please apologise for the inconvenience.';
        }
        return $finalString;
    }

    /**
     * @param $labelBefore
     * @return self
     */
    public function setLabelAfter()
    {
        $this->labelBefore = false;
        return $this;
    }

    public function setDisabled()
    {
        $this->addAttribute('disabled', 'disabled');
    }

    /**
     * @return nameNoPrefix
     */
    public function nameNoPrefix()
    {
        return $this->nameNoPrefix;
    }

    /**
     * @param $nameNoPrefix
     * @return self
     */
    public function setNameNoPrefix($nameNoPrefix)
    {
        $this->nameNoPrefix = $nameNoPrefix;
        return $this;
    }
}
