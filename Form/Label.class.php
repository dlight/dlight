<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
namespace Library\Form;

use \Library\DomElements\DomElementContainer;
use \Library\Form\Field;

class Label extends DomElementContainer
{
    private $field = null;
    private $displayMandatory = true;
    private $text = '';

    public function __construct($field, $text)
    {
        $this->setTagName('label');
        $this->setField($field);
        $this->setText($text);
    }

    private function setField(Field $field)
    {
        $this->field = $field;
        return $this;
    }

    private function setFor()
    {
        $this->addAttribute('for', $this->field->id());
        return $this;
    }

    public function text()
    {
        return $this->text;
    }

    private function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    public function setDisplayMandatoryOff()
    {
        $this->displayMandatory = false;
        return $this;
    }

    public function buildWidget()
    {
        $this->setFor();
        $text = $this->text();
        if ($this->field->isMandatory() && $this->displayMandatory) {
            $text .= ' *';
        }
        parent::addText($text);
        parent::buildWidget();
    }
}
