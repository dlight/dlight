<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Library\Form;

use \Library\Interfaces\IApplication;
use \Library\ApplicationComponent;
use \Library\Form\Form;
use \Library\Entity;

/**
 * Description of FormBuilder
 * Extends ApplicationComponent only to have access to User::csrf method.
 * ? Should the controller set it?
 * ? Should csrf generator not be in User?
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
abstract class FormBuilder extends ApplicationComponent
{
    protected $form = null;
    // @todo remove $entity, should only be needed to Form
    protected $entity = null;

    /**
     * FormBuilder constructor: creates a form object.
     * @param Application $app Used to access User::csrf method.
     * @param Entity $entity The entity to pass to the Form.
     * @param string $formName The name of the Form.
     */
    public function __construct(IApplication $app, Entity $entity, $formName)
    {
        parent::__construct($app);
        $this->form = (new Form($entity, $formName));
        $this->form->setCsrf($this->app->user()->csrf());
        $this->entity = $entity;
    }

    /**
     * Build the Form.
     * Form::add calls are made here.
     */
    abstract public function build();

    /**
     * @return Form
     */
    public function form()
    {
        return $this->form;
    }

    /**
     * Set a prefix for all the fields in self::form.
     * Use this to avoid name conficts when using several identical forms as in CollectionField.
     * This turns csrf creation off.
     * @param string $namePrefix
     * @return self
     */
    public function setFormNamePrefix($namePrefix)
    {
        $this->form->setNamePrefix($namePrefix);
        $this->form->setCreateCsrfOff();
        return $this;
    }

    public function setEntity(Entity $entity)
    {
        $this->entity = $entity;
        $this->form->setEntity($entity);
        return $this;
    }
}
