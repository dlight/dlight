<?php
namespace Library;

class ClassParameter
{
    private $name = '';

    /**
     * @return name
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * @param $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function __tostring()
    {
        return $this->name;
    }
}
