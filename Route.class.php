<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
namespace Library;

/**
 * Description of Route
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
class Route
{
    protected $action;
    protected $bundle;
    protected $url;
    protected $varsNames;
    protected $vars = array();

    public function __construct($url, $bundle, $action, array $varsNames)
    {
        $this->setUrl($url);
        $this->setBundle($bundle);
        $this->setAction($action);
        $this->setVarsNames($varsNames);
    }

    public function hasVars()
    {
        return !empty($this->varsNames);
    }

    public function match($url)
    {
        if (preg_match('`^'.$this->url.'$`', $url, $matches)) {
            return $matches;
        } else {
            return false;
        }
    }

    public function setAction($action)
    {
        if (is_string($action)) {
            $this->action = $action;
        }
    }

    public function setBundle($bundle)
    {
        if (is_string($bundle)) {
            $this->bundle = $bundle;
        }
    }

    public function setUrl($url)
    {
        if (is_string($url)) {
            $this->url = $url;
        }
    }

    public function setVarsNames(array $varsNames)
    {
        $this->varsNames = $varsNames;
    }

    public function setVars(array $vars)
    {
        $this->vars = $vars;
    }

    public function action()
    {
        return $this->action;
    }

    public function bundle()
    {
        return $this->bundle;
    }

    public function vars()
    {
        return $this->vars;
    }

    public function varsNames()
    {
        return $this->varsNames;
    }

    public function hasVar($varName)
    {
        return array_key_exists($varName, $this->vars);
    }

    public function getVar($varName)
    {
        $var = '';
        if ($this->hasVar) {
            $var = $this->vars[$varName];
        }
        return $var;
    }
}
