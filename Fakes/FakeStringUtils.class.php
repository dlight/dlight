<?php
namespace Library\Fakes;

use \Library\Interfaces\IStringUtils;

/**
 * Fake implementation of StringUtils
 **/
class FakeStringUtils implements IStringUtils
{
}
