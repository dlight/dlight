<?php
namespace Library\Fakes;

use \Library\Interfaces\IConfig;

class FakeConfig implements IConfig
{
    public function vars()
    {
    }

    public function get($var)
    {
    }

    public function addVar($varName, $value)
    {
    }

    public function getInt($var)
    {
    }

    public function getString($var)
    {
    }

    public function getBool($var)
    {
    }

    public function timezone()
    {
    }
}
