<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Library;

use \Exception;
use \Library\Interfaces\IFileManager;

/**
 * Manage files.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
class FileManager extends Manager implements IFileManager
{
    private $filePath   = '';
    private $handle     = false;
    private $fileSuffix = '';

    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;
    }

    private function filePath()
    {
        if($this->filePath)
        {
            $this->filePath .= DIRECTORY_SEPARATOR;
        }
        return $this->filePath;
    }

    protected function isFileOpen()
    {
        return $this->handle !== false;
    }

    /**
     * Assign to $this->handle a file pointer resource on success, or false on error.
     * If the open fails, an error of level E_WARNING is generated. You may use @ to suppress this warning.
     * @source http://www.php.net/manual/en/function.fopen.php
     */
    protected function openFile()
    {
        try {
            $this->handle = fopen($this->filePath, 'r');
        } catch (Exception $exception) {
            $this->logger->logException($exception);
            $this->logger->logError("%s:%d: EXCEPTION: Could not open file $this->filePath.", __METHOD__, __LINE__);
        }
        return $this->isFileOpen();
    }

    protected function closeFile()
    {
        if ($this->isFileOpen()) {
            fclose($this->handle);
        }
    }

    /**
     * @return handle
     */
    public function handle()
    {
        return $this->handle;
    }

    public function createFolder($path)
    {
        if (!($isFolderCreated = file_exists($path))) {
            if (($isFolderCreated = mkdir($path, 0755, true))) {
                $this->logger->logMessage(__METHOD__.':'.__LINE__.": folder $path has been created");
            } else {
                $this->logger->logMessage(__METHOD__.':'.__LINE__." could not create folder $path");
            }
        }
        return $isFolderCreated;
    }

    public function fileExists($filename)
    {
        if (!($fileExists = file_exists($filename))) {
            $this->logger->logMessage(__METHOD__.':'.__LINE__.": $filename does not exist.");
        }
        return $fileExists;
    }

    private function fileIsWritable($filename)
    {
        if (!($isWritable = is_writable($filename))) {
            $this->logger->logMessage(__METHOD__.':'.__LINE__.": $filename is not writable.");
        }
        return $isWritable;
    }

    private function setWritePermission($filename)
    {
        if (!chmod($filename, 0600)) {
            $this->logger->logError(__METHOD__.':'.__LINE__." could not give read write permissions to $filename.");
        }
    }

    public function deleteFile($filename)
    {
        $this->logger->logMessage(__METHOD__.':'.__LINE__.": trying to delete $filename...");
        if ($this->fileExists($filename) && $this->fileIsWritable($filename)) {
            $this->setWritePermission($filename);
            if (unlink($filename)) {
                $this->logger->logMessage(__METHOD__.':'.__LINE__.": $filename has been deleted.");
            } else {
                $this->logger->logError(
                    __METHOD__.':'.__LINE__.
                    ": unlink returned false after trying to delete $filename."
                );
            }
        }
    }

    public function deleteFiles(array $filenames)
    {
        foreach ($filenames as $filename) {
            $this->deleteFile($this->filePath().$filename.$this->fileSuffix);
        }
    }
 
    /**
     * @param $fileSuffix
     * @return self
     */
    public function setFileSuffix($fileSuffix)
    {
        $this->fileSuffix = ".$fileSuffix";
        return $this;
    }
}
