<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Library\DomElements;

use \Library\DomElements\DomElement;
use \Library\DomElements\DomElementContainer;
use \Library\DomElements\ListItem;

/**
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
class ListUnordered extends DomElementContainer
{
    private $createItemManually = false;
    private $listItemClasses = array();
    private $childAttributes = array();

    public function addChildClass($class)
    {
        return $this->addListItemClass($class);
    }

    public function addListItemClass($class)
    {
        $this->listItemClasses[] = $class;
        return $this;
    }

    private function resetListItemClasses()
    {
        $this->listItemClasses = array();
    }

    public function addChildAttribute($attributeName, $attributeValue)
    {
        $this->childAttributes[$attributeName] = $attributeValue;
        return $this;
    }

    public function buildWidget()
    {
        $this->setTagName('ul');
        parent::buildWidget();
    }

    private function createListItem()
    {
        return (new ListItem)->setClasses($this->listItemClasses);
    }

    public function addChild(DomElement $child)
    {
        if ($this->createItemManually) {
            return parent::addChild($child);
        } else {
            return $this->finishListItem($this->createListItem()->addChild($child));
        }
    }

    public function addText($text, $separator = '')
    {
        if ($this->createItemManually) {
            return parent::addText($text, $separator);
        } else {
            return $this->finishListItem($this->createListItem()->addText($text));
        }
    }

    private function finishListItem($listItem)
    {
        $listItem->setAttributes($this->childAttributes);
        $this->children[] = $listItem->__tostring();
        $this->resetListItemClasses();
        $this->childAttributes = array();
        return $this;
    }

    /**
     * @param $createItemManually
     * @return self
     */
    public function setCreateItemManually()
    {
        $this->createItemManually = true;
        return $this;
    }
}
