<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
namespace Library\DomElements;

use \Library\DomElements\DomElement;
use \Library\DomElements\TableRow;
use \Library\DomElements\TableCell;
use \Library\Entity;

class Table extends DomElement
{
    private $template = '<table id="$$id$$" class="$$classes$$">
        <caption>$$captionText$$</caption><thead>$$headContent$$</thead><tbody>$$bodyContent$$</tbody></table>';
    private $data = array();
    private $caption = '';
    private $thead = array();
    private $tbody = array();
    private $trTemplate = '<tr>$$cells$$</tr>';
    private $cellClasses = array();
    protected $hasEntity = false;
    public function __construct(array $data, array $classes = array(), $useFirstArrayAsHeader = true)
    {
        $this->data = $data;
        $this->setClasses($classes);
    }
    private function cellHasClasses($cellId)
    {
        return array_key_exists($cellId, $this->cellClasses);
    }
    private function cellClasses($cellId)
    {
        $cellClasses = '';
        if ($this->cellHasClasses($cellId)) {
            $cellClasses = $this->cellClasses[$cellId];
        }
        return $cellClasses;
    }
    public function setCellClasses(array $cellClasses)
    {
        $this->cellClasses = $cellClasses;
    }
    private function firstRow(array &$array)
    {
        $firstRow = array_shift($array);
        if ($firstRow === null) {
            $firstRow = array();
        }
        return $firstRow;
    }
    private function valueFromArray($key, $array)
    {
        $value = array();
        if (array_key_exists($key, $array)) {
            $value = $array[$key];
        }
        return $value;
    }
    /**
     * Create a row of header cells and generate their content.
     * @return string generated header row in HTML
     */
    private function thead()
    {
        return str_replace(
            '$$cells$$',
            implode('', $this->thead),
            $this->trTemplate
        );
    }
    private function setThead(array $headers)
    {
        foreach ($headers as $cellId => $header) {
            $cell = new TableCell(false, $header);
            $cell->addClass($this->cellClasses($cellId));
            $this->thead[] = $cell->__tostring();
        }
    }
    public function setHeader(array $headersRow)
    {
        $this->setThead($headersRow);
    }
    private function tbody()
    {
        $this->setTbody();
        return implode('', $this->tbody);
    }
    //@todo refactor to process object or array, this has been edited with TablePlanning to avoid breaking it
    //when creating contactedcandidates.
    private function setTbody()
    {
        $trCells = array();
        foreach ($this->data as $cellsData) {
            //@see TablePlanning
            if ($this->hasEntity) {
                $cellsData = $this->objectContent($cellsData);
            }
            foreach ($cellsData as $cellId => $value) {
                $cell = new TableCell(true, $this->convertNewLineToBrElement($value));
                $cell->addClass($this->cellClasses($cellId));
                $trCells[] = $cell->__tostring();
            }
            $this->tbody[] = $this->createTableRow($trCells);
            $trCells = array();
        }
    }

    protected function createTableRow(array $cells)
    {
        return (new TableRow($cells))->__tostring();
    }

    protected function objectContent(Entity $entity)
    {
        return array();
    }

    public function buildWidget()
    {
        $this->finalRender = str_replace(
            array(
                '$$id$$',
                '$$classes$$',
                '$$captionText$$',
                '$$headContent$$',
                '$$bodyContent$$',
            ),
            array(
                $this->id(),
                $this->classesString(),
                $this->caption,
                $this->thead(),
                $this->tbody(),
            ),
            $this->template
        );
    }

    /**
     * @param $caption
     * @return self
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;
        return $this;
    }
}
