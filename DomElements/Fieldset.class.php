<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
namespace Library\DomElements;

use Library\DomElements\DomElementContainer;
use Library\DomElements\DomElementTextContainer;

class Fieldset extends DomElementContainer
{
    private $legend = null;

    public function __construct()
    {
        parent::__construct('fieldset');
    }

    public function legend()
    {
        if (!$this->legend) {
            $this->setLegend('');
        }
        return $this->legend;
    }

    public function setLegend($text)
    {
        $this->legend = (new DomElementTextContainer('legend'))->setText($text);
        return $this;
    }

    /**
     * Override DomElementContainer::addchild
     * Ignore child::createcontainer.
     */
    public function addChild(DomElement $child)
    {
        $this->children[] = $child->__tostring();
        return $this;
    }

    protected function buildWidget()
    {
        if ($this->legend) {
            $this->addChild($this->legend());
        }
        parent::buildWidget();
    }
}
