<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Library;

/**
 * Description of HTTPRequest
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
class HTTPRequest
{
    public function cookieData($key)
    {
        return isset($_COOKIE[$key]) ? $_COOKIE[$key] : null;
    }

    public function cookieExists($key)
    {
        return isset($_COOKIE[$key]);
    }

    // @todo make private
    public function getData($key)
    {
        return isset($_GET[$key]) ? $this->sanitizeData($_GET[$key]) : null;
    }

    public function getIntData($key)
    {
        return (int)$this->getData($key);
    }

    public function getStringData($key)
    {
        return (string)$this->getData($key);
    }

    public function getBoolData($key)
    {
        return (bool)$this->getData($key);
    }

    public function getExists($key)
    {
        return isset($_GET[$key]);
    }

    public function method()
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    private function falseToEmpty($data)
    {
        if ($data === false) {
            $data = '';
        }
        return $data;
    }

    /**
     * Sanitize $data.
     * Make a recursive call to itself until it encounters a string.
     * If filter fails, call falseToEmpty() to ensure that an empty string will be returned.
     *
     * @param string|array $data
     * @access private
     * @return string|array
     */
    private function sanitizeData($data)
    {
        if (is_array($data)) {
            foreach ($data as &$value) {
                $value = $this->sanitizeData($value);
            }
            $return = $data;
        } else {
            //$return = $this->falseToEmpty(filter_var(trim((string)$data), FILTER_SANITIZE_MAGIC_QUOTES));
            $return = htmlspecialchars_decode(trim((string)$data));
        }
        return $return;
    }

    /**
     * Return the sanitized value for the HTML form field called $key.
     * If more arguments are supplied, it considers each additional argument as a
     * sub-element of the previous one.
     * @param string $key The name of the HTML form field.
     * @param string ... Names of each nested element.
     * ...
     * @return mixed array|string|null The sanitized value(s) or null.
     */
    public function postData($key)
    {
        $postData = isset($_POST[$key]) ? $this->sanitizeData($_POST[$key]) : null;
        $arguments = func_get_args();
        if (count($arguments) > 1) {
            // Remove first arguments because it has been processed above.
            unset($arguments[0]);
            foreach ($arguments as $argument) {
                if (isset($postData[$argument])) {
                    $postData = $postData[$argument];
                } else {
                    $postData = null;
                }
            }
        }
        return $postData;
    }

    public function postDataArray()
    {
        $args = func_get_args();
        return (($postArray = call_user_func_array(array($this, 'postData'), $args)) ? $postArray : array());
    }

    public function postDataString()
    {
        $args = func_get_args();
        return (string)call_user_func_array(array($this, 'postData'), $args);
    }

    public function postDataInt()
    {
        $args = func_get_args();
        return (int)call_user_func_array(array($this, 'postData'), $args);
    }

    public function postDataBool()
    {
        $args = func_get_args();
        return (bool)call_user_func_array(array($this, 'postData'), $args);
    }

    public function postExists($key)
    {
        return isset($_POST[$key]);
    }

    public function requestURI()
    {
        return $_SERVER['REQUEST_URI'];
    }
}
