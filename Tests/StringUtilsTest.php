<?php
namespace Tests;

use \Library\Utils\StringUtils;

class StringUtilsTest extends \PHPUnit_Framework_TestCase
{
    private $stringUtils = null;
    private $someStringWithSlashes = 'Library/Utils/StringUtils';
    private $someStringWithoutSlashes = 'StringUtils';
    private $someNamespace = 'Library';
    private $someClassNameNotStartingWithAntislash = 'Library\\Utils\\StringUtils';
    private $someFullyQualifiedClassName = '\\Library\\Utils\\StringUtils';
    private $somePhoneNumberTenDigits = '0123456789';
    private $somePhoneNumberElevenDigits = '32491234567';
    private $somePhoneNumberTenDigitsWithSpaces = '0123 456 789';
    private $somePhoneNumberElevenDigitsWithSpaces = '32 491 234 567';
    private $somePhoneNumberTenDigitsWithDots = '0123.456.789';
    private $somePhoneNumberElevenDigitsInternational = '+32.491.234.567';
    private $somePhoneNumberElevenDigitsInternationalWithSpaces = '+32 491 234 567';
    private $somePhoneNumberTwelveDigitsInternationalWithLeadingZero = '+32.(0)491.234.567';
    private $somePhoneNumberTwelveDigitsInternationalWithLeadingZeroWithSpaces = '+32 (0) 491 234 567';
    private $somePhoneNumberWithFullInternationalPrefixInParenthesis = '(+65) 1234 5678';
    private $somePhoneNumberWithFullInternationalPrefixInParenthesisExpectedFormat = '+65 12 345 678';

    public function setUp()
    {
        $this->stringUtils = new StringUtils;
    }

    /**
     * @test
     */
    public function slashesToAntislashes_stringWithSlashes_returnStringWithAntislashes()
    {
        $result = $this->stringUtils->slashesToAntislashes($this->someStringWithSlashes);
        $this->assertEquals('Library\\Utils\\StringUtils', $result);
    }

    /**
     * @test
     */
    public function slashesToAntislashes_stringWithoutSlashes_returnOriginalString()
    {
        $result = $this->stringUtils->slashesToAntislashes($this->someStringWithoutSlashes);
        $this->assertEquals($this->someStringWithoutSlashes, $result);
    }

    /**
     * @test
     */
    public function explodeFullClassName_stringWithSlashes_returnArrayOfTwo()
    {
        $result = $this->stringUtils->explodeFullClassName($this->someStringWithSlashes);
        $this->assertCount(2, $result);
    }

    /**
     * @test
     */
    public function explodeFullClassName_stringWithoutSlashes_returnArrayOfTwo()
    {
        $result = $this->stringUtils->explodeFullClassName($this->someStringWithoutSlashes);
        $this->assertCount(2, $result);
    }

    /**
     * @test
     */
    public function explodeFullClassName_stringWithSlashes_firstCellEqualsNamespace()
    {
        $result = $this->stringUtils->explodeFullClassName($this->someStringWithSlashes);
        $this->assertEquals('Library/Utils', $result[0]);
    }

    /**
     * @test
     */
    public function explodeFullClassName_stringWithSlashes_secondCellEqualsClassName()
    {
        $result = $this->stringUtils->explodeFullClassName($this->someStringWithSlashes);
        $this->assertEquals('StringUtils', $result[1]);
    }

    /**
     * @test
     */
    public function addNamespace_className_returnsStringWithNamespaceBeforeClassName()
    {
        $result = $this->stringUtils->addNamespace($this->someStringWithoutSlashes, $this->someNamespace);
        $this->assertEquals($this->someNamespace.'/'.$this->someStringWithoutSlashes, $result);
    }

    /**
     * @test
     */
    public function prefixFullClassNameWithAntiSlash_classNameNotStartingWithAntislash_returnThatClassNameStartingWithAntislash()
    {
        $result = $this->stringUtils->prefixFullClassNameWithAntiSlash($this->someClassNameNotStartingWithAntislash);
        $this->assertEquals('\\'.$this->someClassNameNotStartingWithAntislash, $result);
    }

    /**
     * @test
     */
    public function className_fullyQualifiedClassName_returnClassNameAlone()
    {
        $result = $this->stringUtils->className($this);
        $this->assertEquals('StringUtilsTest', $result);
    }

    /**
     * @test
     */
    public function nicePhoneNumber_stringTenDigits_returnStringFourDigitsSpaceThreeDigitsSpaceThreeDigits()
    {
        $result = $this->stringUtils->nicePhoneNumber($this->somePhoneNumberTenDigits);
        $this->assertEquals($this->somePhoneNumberTenDigitsWithSpaces, $result);
    }

    /**
     * @test
     */
    public function nicePhoneNumber_stringTenDigitsNicelyFormated_returnThatStringUnmodified()
    {
        $result = $this->stringUtils->nicePhoneNumber($this->somePhoneNumberTenDigitsWithSpaces);
        $this->assertEquals($this->somePhoneNumberTenDigitsWithSpaces, $result);
    }

    /**
     * @test
     */
    public function nicePhoneNumber_stringTenDigitsWithDots_returnFourDigitsSpaceThreeDigitsSpaceThreeDigits()
    {
        $result = $this->stringUtils->nicePhoneNumber($this->somePhoneNumberTenDigitsWithDots);
        $this->assertEquals($this->somePhoneNumberTenDigitsWithSpaces, $result);
    }

    /**
     * @test
     */
    public function nicePhoneNumber_stringElevenDigits_returnTwoDigitsSpaceFollowedByThreeDigitsSpaceSequences()
    {
        $result = $this->stringUtils->nicePhoneNumber($this->somePhoneNumberElevenDigits);
        $this->assertEquals($this->somePhoneNumberElevenDigitsWithSpaces, $result);
    }

    /**
     * @test
     */
    public function nicePhoneNumber_stringElevenDigitsInternational_returnPlusSignTwoDigitsSpaceFollowedByThreeDigitsSpaceSequences()
    {
        $result = $this->stringUtils->nicePhoneNumber($this->somePhoneNumberElevenDigitsInternational);
        $this->assertEquals($this->somePhoneNumberElevenDigitsInternationalWithSpaces, $result);
    }

    /**
     * @test
     */
    public function nicePhoneNumber_stringTwelveDigitsInternationalWithLeadingZeroWithSpaces_returnThatStringUnmodified()
    {
        $result = $this->stringUtils->nicePhoneNumber($this->somePhoneNumberTwelveDigitsInternationalWithLeadingZeroWithSpaces);
        $this->assertEquals($this->somePhoneNumberTwelveDigitsInternationalWithLeadingZeroWithSpaces, $result);
    }

    /**
     * @test
     */
    public function nicePhoneNumber_stringPhoneNumberWithInternationalPrefixInParenthesis_returnFormattedPhoneNumberWithoutParenthesis()
    {
        $result = $this->stringUtils->nicePhoneNumber($this->somePhoneNumberWithFullInternationalPrefixInParenthesis);
        $this->assertEquals($this->somePhoneNumberWithFullInternationalPrefixInParenthesisExpectedFormat, $result);
    }
}
