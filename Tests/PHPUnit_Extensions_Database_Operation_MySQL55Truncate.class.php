<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Library\Tests;

/**
 * Override the execute method of PHPUnit_Extensions_Database_Operation_Truncate class
 * in order to turn off MySQL 5.5+ foreign key checks before the truncate operations
 * occur, and turn them back on after.
 * @source http://www.sitepoint.com/more-bulletproofing-with-phpunit/
 **/
class PHPUnit_Extensions_Database_Operation_MySQL55Truncate extends \PHPUnit_Extensions_Database_Operation_Truncate
{
    public function execute(\PHPUnit_Extensions_Database_DB_IDatabaseConnection $connection, \PHPUnit_Extensions_Database_DataSet_IDataSet $dataSet)
    {
        $connection->getConnection()->query("SET @PHAKE_PREV_foreign_key_checks = @@foreign_key_checks");
        $connection->getConnection()->query("SET foreign_key_checks = 0");
        parent::execute($connection, $dataSet);
        $connection->getConnection()->query("SET foreign_key_checks = @PHAKE_PREV_foreign_key_checks");
    }
}
