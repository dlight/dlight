<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Tests;

use \DateTime;
use \DateTimeZone;
use \Library\Utils\DateUtils;

/**
 * Unit tests for DateUtils.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
class DateUtilsTest extends \PHPUnit_Framework_TestCase
{
    private $dateUtils = null;
    private $someHoliday = null;
    private $someNonHoliday = null;
    private $someHolidaysList = array();
    private $someSaturday = null;
    private $someSunday = null;
    private $someMonday = null;

    public function setUp()
    {
        $this->timezone = new DateTimeZone('Europe/Brussels');
        $this->dateUtils = new DateUtils($this->timezone);
        $this->someHoliday = DateTime::createFromFormat('Ymd', '20141225');
        $this->someNonHoliday = DateTime::createFromFormat('Ymd', '20140805');
        $this->someHolidaysList[] = DateTime::createFromFormat('Ymd', '20140721');
        $this->someHolidaysList[] = DateTime::createFromFormat('Ymd', '20141225');
        $this->someHolidaysList[] = DateTime::createFromFormat('Ymd', '20150101');
        $this->someSaturday = DateTime::createFromFormat('Ymd', '20140809');
        $this->someSunday = DateTime::createFromFormat('Ymd', '20140810');
        $this->someMonday = DateTime::createFromFormat('Ymd', '20140811');
        $this->someDateStringYmd = '20140811';
        $this->someDateStringjn = '1508';
        $this->someCustomDateStringFormat = 'jn';
        $this->someTimezone = new DateTimeZone('Asia/Jakarta');
    }

    /**
     * @test
     */
    public function isNonWorkingDay_holiday_returnTrue()
    {
        $result = $this->dateUtils->isNonWorkingDay($this->someHoliday, $this->someHolidaysList);
        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function isNonWorkingDay_notHoliday_returnFalse()
    {
        $result = $this->dateUtils->isNonWorkingDay($this->someNonHoliday, $this->someHolidaysList);
        $this->assertFalse($result);
    }

    /**
     * @test
     */
    public function isNonWorkingDay_weekendAndIncludeWeekendSet_returnTrue()
    {
        $result = $this->dateUtils->isNonWorkingDay($this->someSunday, $this->someHolidaysList);
        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function isNonWorkingDay_weekendAndIncludeWeekendNotSet_returnFalse()
    {
        $result = $this->dateUtils->isNonWorkingDay($this->someSunday, $this->someHolidaysList, false);
        $this->assertFalse($result);
    }

    /**
     * @test
     */
    public function isNonWorkingDay_weekendHolidayAndIncludeWeekendNotSet_returnTrue()
    {
        $this->someHolidaysList[] = $this->someSunday;
        $result = $this->dateUtils->isNonWorkingDay($this->someSunday, $this->someHolidaysList, false);
        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function weekDayNumber_someSaturday_returnSix()
    {
        $result = $this->dateUtils->weekDayNumber($this->someSaturday);
        $this->assertEquals(6, $result);
    }

    /**
     * @test
     */
    public function weekDayNumber_someSunday_returnZero()
    {
        $result = $this->dateUtils->weekDayNumber($this->someSunday);
        $this->assertEquals(0, $result);
    }

    /**
     * @test
     */
    public function isWeekend_someSaturday_returnTrue()
    {
        $result = $this->dateUtils->isWeekend($this->someSaturday);
        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function isWeekend_someMonday_returnFalse()
    {
        $result = $this->dateUtils->isWeekend($this->someMonday);
        $this->assertFalse($result);
    }

    /**
     * @test
     */
    public function createDate_dateStringInFormatYmd_returnDateTime()
    {
        $result = $this->dateUtils->createDate($this->someDateStringYmd);
        $this->assertInstanceOf('\DateTime', $result);
    }

    /**
     * @test
     */
    public function createDate_dateStringInFormatYmd_returnDateTimeWithTimeSetTo0ByDefault()
    {
        $result = $this->dateUtils->createDate($this->someDateStringYmd);
        $expected = DateTime::createFromFormat('Ymd', $this->someDateStringYmd, $this->timezone)->setTime(0, 0, 0);
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function createDate_dateStringMatchingGivenCustomFormat_returnCorrectDateTimeForThatString()
    {
        $result = $this->dateUtils->createDate($this->someDateStringjn, $this->someCustomDateStringFormat);
        $expected = DateTime::createFromFormat($this->someCustomDateStringFormat, $this->someDateStringjn, $this->timezone)->setTime(0, 0, 0);
        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function createDate_someTimezone_returnDateTimeWithThatTimezone()
    {
        $result = (new DateUtils($this->someTimezone))->createDate($this->someDateStringYmd);
        $expected = DateTime::createFromFormat('Ymd', $this->someDateStringYmd, $this->someTimezone)->setTime(0, 0, 0);
        $this->assertEquals($expected->getTimezone(), $result->getTimezone());
    }
}
