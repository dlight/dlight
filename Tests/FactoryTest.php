<?php
namespace Library\Tests;

use \Library\Factory;

class FactoryTest extends \PHPUnit_Framework_TestCase
{
    private $factory = null;
    private $someKeyName = 'iStringHtmlUtils';
    private $someObjectWithoutDependencies = '\\Library\\Utils\\StringHtmlUtils';
    private $someNonExistantMappingKeyName = 'iFancyInterfaceName';
    private $someClassNameToMap = '\\Library\\Tests\\ClassImplementingNoInterface';
    private $someClassWithDependencies = '\\Library\\User';
    private $someCustomKey = 'test factory';
    private $someClassNameImplementingAnInterface = 'Library\\Fakes\\FakeErrorManager';
    private $loggerInterfaceName = 'Library\\Interfaces\\ILogger';
    private $someClassWithParameters = '\\Library\\Tests\\ClassWithParameters';
    private $someValue = 'the value of a parameter';
    private $someClassWithParametersAndDependencies = '\\Library\\Tests\\ClassWithDependenciesAndParameters';
    private $someClassNotImplementingInterface = '\\Library\\Tests\\ClassImplementingNoInterface';
    private $someRealClassNameNotMapped = '\\Library\\Tests\\ClassWithParameters';
    private $someRealClassNameNotMappedUsingSlashes = 'Library/Tests/ClassWithParameters';

    public function setUp()
    {
        $this->factory = new Factory;
    }

    /**
     * @test
     */
    public function get_validKey_returnStoredObject()
    {
        $this->factory->addObject(new $this->someObjectWithoutDependencies, $this->someKeyName);
        $result = $this->factory->get($this->someKeyName);
        $this->assertInstanceOf($this->someObjectWithoutDependencies, $result);
    }

    /**
     * @test
     */
    public function get_addNewKeyValuePairToMapping_returnInstanceOfClassMatchingThatValueWhenRequestingThatKey()
    {
        $this->factory->addMapping($this->someNonExistantMappingKeyName, $this->someClassNameToMap);
        $result = $this->factory->get($this->someNonExistantMappingKeyName);
        $this->assertInstanceOf($this->someClassNameToMap, $result);
    }

    /**
     * @test
     */
    public function get_classWithDependencies_returnThatClass()
    {
        $this->factory->addObject(new \Library\Fakes\FakeApplication);
        $this->factory->addObject(new \Library\Fakes\FakeArrayUtils, 'Library/Utils/ArrayUtils');
        $this->factory->addObject(new \Library\Fakes\FakeUtils, 'Library/Utils/Utils');
        $this->factory->addMapping($this->someNonExistantMappingKeyName, $this->someClassWithDependencies);
        $result = $this->factory->get($this->someNonExistantMappingKeyName);
        $this->assertInstanceOf($this->someClassWithDependencies, $result);
    }

    /**
     * @test
     */
    public function addObject_someInstanceAndCustomKey_addThatInstanceToFactoryInternalObjectArrayUnderThatKey()
    {
        $this->factory->addObject($this, $this->someCustomKey);
        $result = $this->factory->get($this->someCustomKey);
        $this->assertSame($this, $result);
    }

    /**
     * @test
     */
    public function addObject_someInstanceNoInterfaceNoKey_addThatInstanceToFactoryInternalObjectArrayUsingClassNameAsKey()
    {
        $classNoInterface = new ClassImplementingNoInterface;
        $this->factory->addObject($classNoInterface);
        $result = $this->factory->get(get_class($classNoInterface));
        $this->assertSame($classNoInterface, $result);
    }

    /**
     * @test
     */
    public function addObject_someInstanceImplementingAnInterface_addThatInstanceToFactoryInternalObjectArrayUsingInterfaceNameAsKey()
    {
        $loggerClassName = $this->someClassNameImplementingAnInterface;
        $logger = new $loggerClassName('');
        $this->factory->addObject($logger);
        $result = $this->factory->get($this->loggerInterfaceName);
        $this->assertSame($logger, $result);
    }

    /**
     * @test
     */
    public function get_classWithParameter_returnInstanceOfThatClass()
    {
        $this->factory->config()->addVar('someParam', $this->someValue);
        $this->factory->addMapping($this->someNonExistantMappingKeyName, $this->someClassWithParameters);
        $result = $this->factory->get($this->someNonExistantMappingKeyName);
        $this->assertInstanceOf($this->someClassWithParameters, $result);
    }

    /**
     * @test
     */
    public function get_classWithParameter_parameterIsSet()
    {
        $this->factory->config()->addVar('someParam', $this->someValue);
        $this->factory->addMapping($this->someNonExistantMappingKeyName, $this->someClassWithParameters);
        $result = $this->factory->get($this->someNonExistantMappingKeyName);
        $this->assertEquals($this->someValue, $result->someParam);
    }

    /**
     * @test
     */
    public function get_classWithParameterAndDependency_dependencyIsSet()
    {
        $this->factory->config()->addVar('someParam', $this->someValue);
        $this->factory->addMapping($this->someClassNotImplementingInterface, $this->someClassNotImplementingInterface);
        $this->factory->addMapping($this->someNonExistantMappingKeyName, $this->someClassWithParametersAndDependencies);
        $result = $this->factory->get($this->someNonExistantMappingKeyName);
        $this->assertInstanceOf($this->someClassNotImplementingInterface, $result->someDep);
    }

    /**
     * @test
     */
    public function get_realClassNotMappedNorPreviouslyInstantiated_returnNewInstanceOfThatClass()
    {
        $this->factory->config()->addVar('someParam', $this->someValue);
        $result = $this->factory->get($this->someRealClassNameNotMapped);
        $this->assertInstanceOf($this->someRealClassNameNotMapped, $result);
    }

    /**
     * @test
     */
    public function get_realClassNotMappedUsingSlashes_returnNewInstanceOfThatClass()
    {
        $this->factory->config()->addVar('someParam', $this->someValue);
        $result = $this->factory->get($this->someRealClassNameNotMappedUsingSlashes);
        $this->assertInstanceOf($this->someRealClassNameNotMapped, $result);
    }

    /**
     * @test
     * @expectedException \Library\Errors\ClassNotFoundException
     */
    public function get_nonExistantClassName_throwsClassNotFoundException()
    {
        $result = $this->factory->get('idonotexist');
    }

    /**
     * @test
     */
    public function getFake_someClass_returnFakeInstanceOfThatClass()
    {
        $result = $this->factory->getFake('Cache');
        $this->assertInstanceOf('\\Library\\Fakes\\FakeCache', $result);
    }
}
class ClassImplementingNoInterface {}
class ClassWithParameters
{
    public $someParam = '';

    public function __construct($someParam)
    {
        $this->someParam = $someParam;
    }
}
class ClassWithDependenciesAndParameters extends ClassWithParameters
{
    public $someDep = null;

    public function __construct($someParam, ClassImplementingNoInterface $someDep)
    {
        parent::__construct($someParam);
        $this->someDep = $someDep;
    }
}
