<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Library;

use \Library\ApplicationComponent;
use \Library\Interfaces\IApplication;
use \Library\Utils\ArrayUtils;
use \Library\Utils\Utils;

/**
 * Description of User
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
class User extends ApplicationComponent
{
    private $arrayUtils = null;
    private $utils = null;
    private $errorMessage = ' This error has been logged and will be investigated.';
    private $sessionMaxUses = 10;

    public function __construct(IApplication $app, ArrayUtils $arrayUtils, Utils $utils)
    {
        parent::__construct($app);
        $this->arrayUtils = $arrayUtils;
        $this->utils = $utils;
        $this->init();
    }

    protected function init()
    {
        $this->initSession();
    }

    private function initSession()
    {
        $this->startSession();
        if (!$this->sessionData('returningSession')) {
            $this->app->errorManager()->logMessage(__METHOD__.':'.__LINE__.': this is a fresh session.');
            $this->resetSession();
            $_SESSION['returningSession'] = true;
            $_SESSION['IP'] = $this->arrayUtils->serverData('REMOTE_ADDR');
            $_SESSION['userAgent'] = $this->arrayUtils->serverData('HTTP_USER_AGENT');
            $this->initCsrf();
        } else {
            $this->app->errorManager()->logMessage(__METHOD__.':'.__LINE__.': this is a returning session.');
            $sessionValidLogMessage = ': this session '.session_id().' is %svalid.';
            if ($this->isSessionValid()) {
                $this->app->errorManager()->logMessage(__METHOD__.':'.__LINE__.sprintf($sessionValidLogMessage, ''));
                $this->regenerateSessionId();
            } else {
                $this->app->errorManager()->logError(__METHOD__.':'.__LINE__.sprintf($sessionValidLogMessage, 'not '));
                $this->destroySession();
            }
        }
    }

    private function sessionData($attributeName)
    {
        if (isset($_SESSION[$attributeName])) {
            return $_SESSION[$attributeName];
        } else {
            $this->app->errorManager()->logMessage(
                __METHOD__.':'.__LINE__.
                ": $attributeName does not exist in SESSION superglobal."
            );
            return false;
        }
    }

    private function isSessionValid()
    {
        $isSessionValid = true;
        if ($this->sessionData('obsolete') && $this->sessionData('lifetime') < time()) {
            $isSessionValid = false;
            $this->app->errorManager()->logError(__METHOD__.':'.__LINE__.': session '.session_id().' has expired.');
        }
        if (($storedIp = $this->sessionData('IP')) != ($newIp = $_SERVER['REMOTE_ADDR'])) {
            $isSessionValid = false;
            $this->app->errorManager()->logError(
                __METHOD__.':'.__LINE__.": stored IP $storedIp does not match $newIp."
            );
        }
        if (($storedUserAgent = $this->sessionData('userAgent')) != ($newUserAgent = $_SERVER['HTTP_USER_AGENT'])) {
            $isSessionValid = false;
            $this->app->errorManager()->logError(
                __METHOD__.':'.__LINE__.
                ": stored user agent $storedUserAgent does not match $newUserAgent."
            );
        }
        if (($nbTimesUsed = $this->sessionData('nbTimesUsed')) > $this->sessionMaxUses) {
            $isSessionValid = false;
            $this->app->errorManager()->logError(
                __METHOD__.':'.__LINE__.': session '.session_id()." has been used $this->sessionMaxUses times."
            );
        }
        $this->increaseSessionUsesCounter();
        return $isSessionValid;
    }

    private function increaseSessionUsesCounter()
    {
        $_SESSION['nbTimesUsed'] = $this->sessionData('nbTimesUsed') + 1;
    }

    private function resetSessionUsesCounter()
    {
        $_SESSION['nbTimesUsed'] = 0;
    }

    private function isSessionStarted()
    {
        $isSessionStarted = false;
        if (version_compare(phpversion(), '5.4.0', '>=')) {
            $isSessionStarted = session_status() === PHP_SESSION_ACTIVE ? true : false;
        } else {
            $isSessionStarted = session_id() === '' ? false : true;
        }
        return $isSessionStarted;
    }

    private function startSession($sessionId = 0)
    {
        if ($this->isSessionStarted()) {
            $this->app->errorManager()->logMessage(
                __METHOD__.':'.__LINE__.
                ': a session with ID '.session_id().' has already started.'
            );
        } else {
            if ($sessionId) {
                session_id($sessionId);
            }
            session_start();
            $this->app->errorManager()->logMessage(
                __METHOD__.':'.__LINE__.': session started with ID '.session_id().'.'
            );
        }
        $this->app->errorManager()->logSession();
    }

    private function resetSession()
    {
        session_unset();
    }

    public function destroySession()
    {
        $this->resetSession();
        session_destroy();
    }

    private function regenerateSessionId()
    {
        // Set the current session to expire in 1 minute.
        $_SESSION['obsolete'] = true;
        $_SESSION['lifetime'] = time() + 60;
        $this->app->errorManager()->logMessage(
            __METHOD__.':'.__LINE__.': the session '.session_id().' has been marked as obsolete.'
        );
        // Create a new session without destroying the old one.
        session_regenerate_id(false);
        $this->app->errorManager()->logMessage(
            __METHOD__.':'.__LINE__.': session ID has been regenerated and is now '.session_id()
        );
        // Store current new session ID, so we can start the right session after closing everything.
        $newSessionId = session_id();
        // Close both sessions to allow other scripts to use them.
        session_write_close();
        $this->app->errorManager()->logMessage(__METHOD__.':'.__LINE__.': all sessions have been closed.');
        // Restart the session with the regenerated ID.
        $this->startSession($newSessionId);
        // Delete the expiration data created for the previous session, so this one will not be affected.
        unset($_SESSION['obsolete']);
        unset($_SESSION['lifetime']);
        $this->resetSessionUsesCounter();
    }

    public function getStringAttribute($attributeName)
    {
        return $this->getAttribute($attributeName, 'string');
    }

    // $_SESSION return the right type! I make sure it is the good type in case the session file is tempered with.
    public function getIntAttribute($attributeName)
    {
        return isset($_SESSION[$attributeName]) ? (int)$_SESSION[$attributeName] : 0;
    }

    public function getBoolAttribute($attributeName)
    {
        return isset($_SESSION[$attributeName]) ? (bool)$_SESSION[$attributeName] : false;
    }

    public function getArrayAttribute($attributeName)
    {
        return $this->getAttribute($attributeName, 'array');
    }

    private function getAttribute($attributeName, $attributeType = 'string')
    {
        if (isset($_SESSION[$attributeName]) && gettype($_SESSION[$attributeName]) == $attributeType) {
            $attributeValue = $_SESSION[$attributeName];
        } else {
            $attributeValue = $this->utils->defaultValue($attributeType);
        }
        return $attributeValue;
    }

    public function flash()
    {
        return $this->getFlash();
    }

    public function getFlash()
    {
        $this->initFlash();
        $flash = $_SESSION['flash'];
        unset($_SESSION['flash']);
        return $flash;
    }

    public function hasFlash()
    {
        return isset($_SESSION['flash']);
    }

    public function isAuthenticated()
    {
        return isset($_SESSION['auth']) && $_SESSION['auth'] === true;
    }

    public function setAttributes(array $attributes)
    {
        foreach ($attributes as $attribute => $value) {
            $this->addAttribute($attribute, $value);
        }
    }

    public function addAttribute($attr, $value)
    {
        $_SESSION[$attr] = $value;
    }

    public function setAuthenticated()
    {
        $_SESSION['auth'] = true;
    }

    public function csrf()
    {
        $this->initCsrf();
        return $_SESSION['csrf'];
    }

    private function initCsrf()
    {
        if (!isset($_SESSION['csrf'])) {
            $_SESSION['csrf'] = bin2hex(openssl_random_pseudo_bytes(71));
            $this->app->errorManager()->logMessage(__METHOD__.':'.__LINE__.": a new csrf has been generated.");
        }
    }

    /**
     * Compare $csrf to the one stored.
     * Generate a new one.
     * @param string $csrf
     * @return bool
     */
    public function isCsrfValid($csrf)
    {
        $isCsrfValid = false;
        $this->initCsrf();
        if ($_SESSION['csrf'] === $csrf) {
            $isCsrfValid = true;
            $this->app->errorManager()->logMessage(
                __METHOD__.':'.__LINE__.": form csrf is valid: $csrf does match stored csrf {$_SESSION['csrf']}."
            );
        } else {
            $this->app->errorManager()->logMessage(
                __METHOD__.':'.__LINE__.": form csrf is invalid: $csrf does not match stored csrf {$_SESSION['csrf']}."
            );
        }
        return $isCsrfValid;
    }

    public function setFlash(array $messages)
    {
        foreach ($messages as $message) {
            $this->addFlash($message);
        }
    }

    private function initFlash()
    {
        if (!isset($_SESSION['flash'])) {
            $this->resetFlash();
        }
    }

    public function addFlash($message)
    {
        $this->initFlash();
        $trace = list(,$caller) = debug_backtrace(false);
        //var_dump($trace);die;
        $caller = $trace[0]['file'].':'.$trace[0]['line'];
        $this->app->errorManager()->logMessage(
            __METHOD__.':'.__LINE__.": New flash message set by $caller: \"$message\"."
        );
        $_SESSION['flash'][] = $message;
    }

    public function addFlashError($message)
    {
        $this->addFlash($message.$this->errorMessage);
    }

    public function resetFlash()
    {
        $_SESSION['flash'] = array();
    }

    public function roleForBundle($bundleId)
    {
        $roleCode = 9999;
        $defaultCode = $roleCode;
        foreach ($this->roles() as $role) {
            if ($role->bundleId() == $bundleId) {
                $roleCode = $role->code();
                break;
            } elseif ($role->bundleId() == 0) {
                $defaultCode = $role->code();
            }
        }
        if ($roleCode == 9999) {
            $roleCode = $defaultCode;
        }
        return $roleCode;
    }

    private function roles()
    {
        return $this->getArrayAttribute('roles');
    }
}
