<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Library;

use \Exception;
use \Library\Cache;
use \Library\Config;
use \Library\Errors\ClassNotFoundException;
use \Library\ErrorManager;
use \Library\Fakes\FakeErrorManager;
use \Library\FileManagers\XmlFileManager;
use \Library\Interfaces\IFactory;
use \Library\Utils\StringUtils;
use \ReflectionClass;

/**
 * Main factory.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
class Factory implements IFactory
{
    private $cachePath = 'Cache';
    private $iocMappings = array();
    private $builtObjects = array();
    private $fakesDirectoryPath = 'Library/Fakes';
    protected $logger = null;
    protected $config = null;
    protected $stringUtils = null;

    /**
     * Init config, logger, stringUtils and xmlFileManager.
     * Init default object mappings using iocMappings parameter in config.
     */
    public function __construct()
    {
        require('rootConfiguration.php');
        $this->cache = new Cache((new FakeErrorManager('')), $cachePath);
        $this->config = new Config;
        $this->config->setCache($this->cache);
        $this->logger = new ErrorManager($this->config->getString('logsPath'), !$this->config->getString('production'));
        $this->logger->setTimezone($this->config->timezone());
        $this->logger->logMessage(__METHOD__.':'.__LINE__.": START!");
        $this->logger->logServerInfos();
        $this->cache->setLogger($this->logger);
        $this->stringUtils = new StringUtils;
        // addObject requires stringUtils
        $this->addObject($this, 'Library/Interfaces/IFactory');
        $this->addObject($this->config);
        $this->addObject($this->logger);
        $this->addObject($this->stringUtils);
        $this->initIocMappings();
    }

    private function initIocMappings()
    {
        $xmlFileManager = new XmlFileManager($this->config, $this->logger, $this->cache, $this->stringUtils);
        $this->addObject($xmlFileManager);
        $cacheFilename = 'Factory-'.__FUNCTION__;
        if (!($this->iocMappings = $this->cache->readArray($cacheFilename))) {
            $this->iocMappings = $xmlFileManager->parseXmlToKeyValueArray(
                $this->config->getString('iocMappings'),
                'key',
                'class'
            );
            $this->cache->writeArray($cacheFilename, $this->iocMappings);
        }
    }

    /**
     * Returns a fake object for $className.
     * Currently support only fakes stored in Library/Fakes.
     * @param string $className The name of the class to fake. No namespace.
     * @return object Instance of the fake object.
     */
    public function getFake($className)
    {
        return $this->get($this->fakesDirectoryPath."/Fake$className");
    }

    public function get($key)
    {
        $instance = null;
        $dependencies = array();
        $antislashedKey = $this->getValidFullyQualifiedClassName($key);
        $this->logger->logDebug(__METHOD__.':'.__LINE__.": $key has been converted to $antislashedKey.");
        if (!($instance = $this->getInstance($antislashedKey))) {
            if (!($fullClassName = $this->getFullClassNameFromMapping($antislashedKey))) {
                $fullClassName = $antislashedKey;
                $this->logger->logDebug(
                    __METHOD__.':'.__LINE__.": trying to use $fullClassName".
                    " directly as a real class name."
                );
            }
            $dependencies = $this->getDependencies($fullClassName);
            $instance = $this->buildInstance($fullClassName, $dependencies);
        }
        if (!$instance) {
            $this->logger->logError(__METHOD__.':'.__LINE__.": could not get an instance of $antislashedKey.");
            throw new ClassNotFoundException;
        }
        return $instance;
    }

    private function getFullClassNameFromMapping($antislashedKey)
    {
        $fullClassName = '';
        if (array_key_exists($antislashedKey, $this->iocMappings)) {
            $fullClassName = $this->iocMappings[$antislashedKey];
            $this->logger->logDebug(__METHOD__.':'.__LINE__.": $antislashedKey is mapped to $fullClassName.");
        } else {
            $this->logger->logDebug(__METHOD__.':'.__LINE__.": $antislashedKey is not mapped.");
        }
        return $fullClassName;
    }

    private function buildInstance($fullClassName, array $args)
    {
        $key = $fullClassName;
        if ($reflectionClass = $this->getReflection($fullClassName)) {
            try {
                $key = $this->addObject($reflectionClass->newInstanceArgs($args));
                $this->logger->logDebug(__METHOD__.':'.__LINE__.": an instance of $fullClassName has been created.");
            } catch (Exception $exception) {
                $this->logger->logException($exception);
                $this->logger->logError(__METHOD__.':'.__LINE__.": could not instantiate $fullClassName.");
            }
        }
        return $this->getInstance($key);
    }

    private function getReflection($fullClassName)
    {
        $reflectionClass = null;
        try {
            $reflectionClass = new ReflectionClass($fullClassName);
        } catch (Exception $exception) {
            $this->logger->logException($exception);
        }
        return $reflectionClass;
    }

    private function getDependencies($fullClassName)
    {
        $this->logger->logDebug(__METHOD__.':'.__LINE__.": dependencies requested for $fullClassName.");
        $dependencies = array();
        try {
            if ($reflectionClass = $this->getReflection($fullClassName)) {
                $constructor = $reflectionClass->getConstructor();
                if ($constructor) {
                    $constructorParameters = $constructor->getParameters();
                    foreach ($constructorParameters as $reflectionParameter) {
                        $dependency = $reflectionParameter->getClass();
                        // If null, it is a parameter.
                        if ($dependency) {
                            $dependencyClassName = $dependency->name;
                            $this->logger->logDebug(
                                __METHOD__.':'.__LINE__.": dependency".
                                " $dependencyClassName needed for $fullClassName."
                            );
                            $dependencies[] = $this->get($dependencyClassName);
                        } else {
                            $parameterName = $reflectionParameter->getName();
                            $this->logger->logDebug(
                                __METHOD__.':'.__LINE__.": parameter $parameterName".
                                " needed for $fullClassName."
                            );
                            if ($parameter = $this->config->get($parameterName)) {
                                $dependencies[] = $parameter;
                            } else {
                                $logLevel = 'Error';
                                $parameterType = '';
                                if ($reflectionParameter->isOptional()) {
                                    $logLevel = 'Debug';
                                    $parameterType = 'optional ';
                                }
                                $logMethod = 'log'.$logLevel;
                                $this->logger->$logMethod(
                                    __METHOD__.':'.__LINE__.": {$parameterType}parameter".
                                    " $parameterName not found in configuration."
                                );
                            }
                        }
                    }
                }
            }
        } catch (Exception $exception) {
            $this->logger->logException($exception);
        }
        return $dependencies;
    }

    private function getInstance($key)
    {
        $instance = null;
        $this->logger->logDebug(__METHOD__.':'.__LINE__.": an instance of $key has been requested.");
        if (array_key_exists($key, $this->builtObjects)) {
            $this->logger->logDebug(__METHOD__.':'.__LINE__.": an instance of $key exists.");
            $instance = $this->builtObjects[$key];
        } else {
            $this->logger->logDebug(__METHOD__.':'.__LINE__.": there is no saved instance for $key.");
        }
        return $instance;
    }

    private function getValidFullyQualifiedClassName($fullClassName)
    {
        return $this->stringUtils->prefixFullClassNameWithAntiSlash(
            $this->stringUtils->slashesToAntislashes($fullClassName)
        );
    }

    public function addObject($object, $key = '')
    {
        if (!$key) {
            $key = get_class($object);
            if ($reflection = $this->getReflection($key)) {
                if ($interfaceNames = $reflection->getInterfaceNames()) {
                    $key = $interfaceNames[0];
                }
            }
        }
        $key = $this->getValidFullyQualifiedClassName($key);
        $this->builtObjects[$key] = $object;
        $this->logger->logDebug(
            __METHOD__.':'.__LINE__.": instance of type ".get_class($object).
            " saved at key $key"
        );
        return $key;
    }

    public function addMapping($key, $fullClassName)
    {
        $key = $this->getValidFullyQualifiedClassName($key);
        $fullClassName = $this->getValidFullyQualifiedClassName($fullClassName);
        $this->iocMappings[$key] = $fullClassName;
        $this->logger->logDebug(__METHOD__.':'.__LINE__.": $fullClassName has been mapped to $key.");
    }

    public function config()
    {
        return $this->config;
    }
}
