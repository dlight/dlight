<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Library;

use \DateTime;
use \DateTimeZone;
use \Exception;
use \Library\Interfaces\ILogger;
use \Library\Utils\ArrayUtils;

/**
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
class ErrorManager implements ILogger
{
    private $logPath = '';
    private $debug = false;
    private $logFiles = array();
    private $messageParts = array();
    private $isSuccess = false;
    private $timezone = null;

    /**
     * @todo pass all dependencies as parameters and set them.
     * Constructor shouldn't do any logic.
     * Logic should be in its own methods so we can test them.
     */
    public function __construct($logPath, $debug = false)
    {
        if (!($this->logPath = $logPath)) {
            die('This application is not properly configured: no log path.');
        }
        if (php_sapi_name() === 'cli') {
            $this->logPath .= '/cli';
            $this->setMainLogFile('script');
            $this->setErrorLogFile('script_errors');
        } else {
            $this->logPath .= '/web';
        }
        $this->addLogFile('errors');
        $this->addLogFile('main');
        $this->addLogFile('access');
        $this->setDebug($debug);
    }

    public function setMainLogFile($logFilename)
    {
        $this->addLogFile($logFilename, 'main');
    }

    public function setErrorLogFile($logFilename)
    {
        $this->addLogFile($logFilename, 'errors');
    }

    private function logFile($logName)
    {
        $logFile = '';
        if (array_key_exists($logName, $this->logFiles)) {
            $logFile = $this->logFiles[$logName];
        } else {
            throw new Exception("Log filename for $logName is empty.");
        }
        return $this->logPath.'/'.$logFile;
    }

    private function addLogFile($logFilename, $logName = '')
    {
        $this->logFiles[($logName ? $logName : $logFilename)] = "$logFilename.log";
        return $this;
    }

    private function initLogDirectory()
    {
        if (!file_exists($this->logPath)) {
            try {
                mkdir($this->logPath);
            } catch (Exception $exception) {
                die('Could not create log directory. Aborting.');
            }
        } else {
            //$this->setPermissions($this->logPath, 0744);
        }
    }

    private function setPermissions($filepath, $permissionsOctal = 0644)
    {
        if ((int)substr(sprintf('%o', fileperms($filepath)), -4) != $permissionsOctal) {
            if (!chmod($filepath, $permissionsOctal)) {
                die('Log directory permissions are not properly configured and could not be changed. Aborting.');
            }
        }
    }

    private function init()
    {
        $this->initLogDirectory();
        //foreach($this->logFiles as $logFile)
        //{
            //$logFilePath = $this->logPath.'/'.$logFile;
            //if(!file_exists($logFilePath))
            //{
                //var_dump($logFilePath);
                //if(!touch($logFilePath))
                //{
                    //die('Could not create log file.');
                //}
            //}
            //$this->setPermissions($logFilePath);
        //}
    }

    private function logAccess()
    {
        $this->writeLog('access', json_encode($this->serverLight()));
    }

    private function writeLog($logName, $message)
    {
        $this->init();
        try {
            $logFullPath = $this->logFile($logName);
        } catch (Exception $exception) {
            $this->logException($exception);
        }
        try {
            file_put_contents(
                $logFullPath,
                $this->currentDateString().$this->timeElapsed().' --> '.$message.PHP_EOL,
                FILE_APPEND | LOCK_EX
            );
        } catch (Exception $exception) {
            $errorMessage = __METHOD__.": exception thrown by file_put_contents trying to write in $logFullPath. ".
                $exception->__tostring();
            echo $errorMessage;
            //$command = 'echo "'.addslashes($errorMessage).'" >> '.$this->errorsLogPath().' 2>&1 &';
            //exec($command);
        }
    }

    /**
     * Insert each passed parameters in a new cell of $this->messageParts.
     */
    private function addMessagePart()
    {
        foreach (func_get_args() as $messagePart) {
            $this->messageParts[] = is_array($messagePart) ? (new ArrayUtils)->recursiveImplode(
                PHP_EOL,
                $messagePart,
                '',
                true
            ) : trim($messagePart);
        }
    }

    public function logSession()
    {
        $this->messageParts = array();
        $this->logSuperglobal('SESSION');
        $this->writeLog('main', implode(' ### ', $this->messageParts));
    }

    public function logServerInfos()
    {
        $this->logSuperglobal('GET');
        $this->logSuperglobal('POST');
        $this->logSuperglobal('FILES');
        $this->logSuperglobal('SESSION');
        $this->logSuperglobal('COOKIE');
        //$this->logSuperglobal('SERVER_LIGHT');
        $this->logConfigPhp();
        $this->writeLog('main', implode(' ### ', $this->messageParts));
        $this->logAccess();
        //$this->browserInfos();
    }

    private function logConfigPhp()
    {
        $this->addMessagePart('PHP :: session.use_trans_sid = '.ini_get('session.use_trans_sid'));
    }

    // Require browscap.ini
    private function browserInfos()
    {
        $browser = get_browser();
        // javascript only tells if browser support it, not if it is enabled
        $this->logMessage(
            $browser->browser.' '.$browser->majorver.'.'.$browser->minorver.' on '.$browser->platform.'; cookies:'.
            $browser->cookies
        );
    }

    private function serverLight()
    {
        $keysToFetch = array(
            'HTTP_USER_AGENT',
            'REMOTE_ADDR',
            'HTTP_REFERER',
            'SCRIPT_FILENAME',
            'REQUEST_URI',
            'REQUEST_TIME',
            'HTTPS',
            'HTTP_X_REQUESTED_WITH',
        );
        $server = array();
        foreach ($keysToFetch as $keyToFetch) {
            if (isset($_SERVER[$keyToFetch])) {
                $server[$keyToFetch] = $_SERVER[$keyToFetch];
            }
        }
        if (isset($server['REQUEST_TIME'])) {
            $server['REQUEST_TIME'] = date('Y-m-d @ H:i:s', $server['REQUEST_TIME']);
        }
        return $server;
    }

    /**
     * Variable variables cannot be used with superglobal: http://php.net/manual/en/language.variables.variable.php
     */
    private function logSuperglobal($variableName)
    {
        $validSuperglobals = array(
            'COOKIE' => $_COOKIE,
            'FILES' => $_FILES,
            'GET' => $_GET,
            'POST' => $this->filterPost(),
            'SESSION' => isset($_SESSION) ? $_SESSION : 'Session not set.',
            'SERVER' => $_SERVER,
            'SERVER_LIGHT' => $this->serverLight(),
        );
        if (array_key_exists($variableName, $validSuperglobals)) {
            $this->addMessagePart("$variableName :: ".json_encode($validSuperglobals[$variableName]));
        }
    }

    /**
     * Replace sensitive data like password with dummy to avoid logging them in plain text.
     * @return string
     */
    private function filterPost()
    {
        $postData = $_POST;
        if (isset($postData['password'])) {
            $postData['password'] = strlen($postData['password']) > 0 ? 'Password submitted' : 'Empty password';
        }
        return $postData;
    }

    private function currentDateString()
    {
        return (new DateTime('now', $this->timezone))->format('Y-m-d @ H:i:s');
    }

    private function timeElapsed()
    {
        if (isset($_SERVER['REQUEST_TIME_FLOAT'])) {
            $message = number_format(microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'], 5).' elapsed';
        } else {
            $message = '$_SERVER[\'REQUEST_TIME_FLOAT\'] is not set';
        }
        return " ($message)";
    }

    public function logDebug($message)
    {
        if ($this->debug) {
            $this->writeLog('main', $message);
        }
    }

    public function logMessage($message)
    {
        $this->writeLog('main', $message);
    }

    public function logError($message)
    {
        $this->writeLog('errors', $message);
    }

    public function logException(Exception $exception)
    {
        $this->writeLog(
            'errors',
            'EXCEPTION CATCHED: '.$exception->getFile().':'.$exception->getLine().':'.$exception->getMessage().PHP_EOL.
            $exception->getTraceAsString()
        );
    }

    public function emptyLog($logname)
    {
        $filename = '';
        try {
            $filename = $this->logFile($logname);
            file_put_contents($filename, '');
            $this->isSuccess = true;
        } catch (Exception $exception) {
            $this->logException($exception);
            if ($filename) {
                $this->logError(__METHOD__.':'.__LINE__.": Could not write in $filename.");
            }
            $this->isSuccess = false;
        }
    }

    public function emptyLogs()
    {
        $isSuccess = true;
        $this->initLogDirectory();
        foreach ($this->logFiles as $logname => $filename) {
            $this->emptyLog($logname);
            if ($isSuccess && !$this->isSuccess) {
                $isSuccess = false;
            }
        }
        return $isSuccess;
    }

    /**
     * @param $timezone
     * @return self
     */
    public function setTimezone(DateTimeZone $timezone)
    {
        $this->timezone = $timezone;
        return $this;
    }

    private function setDebug($debug)
    {
        if ($debug) {
            $this->debug = true;
            $this->logMessage(__METHOD__.'.'.__LINE__.': debug is set.');
        }
        return $this;
    }
}
