<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 */
namespace Library;

use \ArrayAccess;
use \DateTime;
use \Exception;

/**
 * Description of Entity
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
abstract class Entity implements ArrayAccess
{
    private $includeAttributes = array();
    private $unsetAttributes = array();
    private $invertUnsetAttributes = false;
    protected $errors = array();
    protected $id = 0;

    public function __construct(array $data = array())
    {
        if (!empty($data)) {
            $this->hydrate($data);
        }
    }

    public function isNew()
    {
        return empty($this->id);
    }

    abstract public function isValid();

    public function errors()
    {
        return $this->errors;
    }

    protected function addError($errorMessage, $attributeName)
    {
        $this->errors[$attributeName] = $errorMessage;
    }

    public function id()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = (int) $id;
    }

    public function includeAttribute($attributeName)
    {
        $this->includeAttributes[] = $attributeName;
    }

    public function addUnsetAttribute($attributeName)
    {
        $this->unsetAttributes[] = $attributeName;
    }

    public function setUnsetAttributes(array $attributeNames)
    {
        $this->unsetAttributes = $attributeNames;
    }

    public function invertUnsetAttributes()
    {
        $this->invertUnsetAttributes = true;
    }

    public function resetUnsetAttributes()
    {
        $this->unsetAttributes = array();
    }

    public function hydrate(array $data)
    {
        foreach ($data as $attribute => $value) {
            $method = 'set'.ucfirst($attribute);
            if (is_callable(array($this, $method))) {
                $this->$method($value);
            }
        }
    }

    public function offsetGet($var)
    {
        if (isset($this->$var) && is_callable(array($this, $var))) {
            return $this->$var();
        }
    }

    public function offsetSet($var, $value)
    {
        $method = 'set'.ucfirst($var);
        if (isset($this->$var) && is_callable(array($this, $method))) {
            $this->$method($value);
        }
    }

    public function offsetExists($var)
    {
        return isset($this->$var) && is_callable(array($this, $var));
    }

    public function offsetUnset($var)
    {
        throw new Exception('Cannot delete some value.');
    }

    public function attributeNames()
    {
        if ($this->invertUnsetAttributes) {
            $attributeNames = $this->unsetAttributes;
        } else {
            //get_called_class: Gets the name of the class the static method is called in.
            $attributeNames = get_class_vars(get_called_class());
            unset($attributeNames['errors'], $attributeNames['includeAttributes'],
                $attributeNames['unsetAttributes'], $attributeNames['invertUnsetAttributes']);
            //remove class static attributes and attributes specified in the class' $attributesToExclude array
            foreach ($attributeNames as $attributeName => $attributeValue) {
                //self:: is this Entity class, static it the class from which this method is called
                if (isset(self::$$attributeName) || isset(static::$$attributeName)) {
                    unset($attributeNames[$attributeName]);
                }
            }
            //keys become values
            $attributeNames = array_diff(array_keys($attributeNames), $this->unsetAttributes);
            $attributeNames = array_merge($attributeNames, $this->includeAttributes);
        }
        return $attributeNames;
    }

    public function attributeNamesListUpdate()
    {
        $attributeNames = $this->attributeNamesList();
        foreach ($this->unsetAttributesUpdate() as $attributeName) {
            if (($key = array_search($attributeName, $attributeNames)) !== false) {
                unset($attributeNames[$key]);
            }
        }
        return $attributeNames;
    }

    /**
     * Return the class attributes and their type.
     * When null is returned, it means the type is an object. So the raw type must be an integer as ID.
     * The exception are dates, which must be kept as strings to be used to create a DateTime objects.
     *
     * @access public
     * @return array attribute names as keys, their type as values
     */
    public function rawTypes()
    {
        //return properties of this object
        $properties = get_class_vars(get_class($this));
        unset($properties['id']);
        unset($properties['errors']);
        foreach ($properties as $propertyName => &$propertyValue) {
            if (method_exists($this, $propertyName)) {
                $propertyValue = gettype($this->$propertyName());
                if ($propertyValue == 'object') {
                    $className = get_class($this->$propertyName());
                    if (($lastSeparatorPosition = strrpos($className, '\\')) !== false) {
                        $className = substr($className, $lastSeparatorPosition + 2);
                    }
                    switch($className)
                    {
                        case 'DateTime':
                            if (($date = DateTime::createFromFormat('Y-m-d H:i:s', $propertyValue)) !== false) {
                                $propertyValue = 'string';
                            }
                            break;
                        default:
                            $propertyValue = 'integer';
                            break;
                    }
                }
            }
        }
        return $properties;
    }

    /**
     * Returns the content of the array targeted by $listName in the current class.
     * Remove the first value which should be an empty one (@see Opportunity, Offer).
     * Only look for static methods.
     * @todo refactor code in various entities.
     */
    public function listForFilter($listName)
    {
        $list = array();
        $className = get_class($this);
        $methodName = $listName.'List';
        if (method_exists($className, $methodName)) {
            $list = $className::$methodName();
            //var_dump($list);
            //array_shift($list);
            //preserve numeric keys:
            $list = array_slice($list, 1, null, true);
        }
        //var_dump($list);
        return $list;
    }

    public function isDateValid($dateAttributeName)
    {
        // Check the attribute, do not call $dateAttributeName() since this method is often called by it!
        return (int)$this->$dateAttributeName->format('Y') > 1900;
    }

    public function hasAttachment($attachmentAttributeName)
    {
        return $this->$attachmentAttributeName() != '';
    }

    public function dateAsString($attributeName, $format = 'M d, Y - H:i:s')
    {
        $dateString = '';
        if (method_exists($this, $attributeName)) {
            $dateString = $this->$attributeName()->format($format);
        }
        return $dateString;
    }
}
