<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
namespace Library;

// @todo create default Language entity
use \Application\Entities\Language;
use \Exception;

abstract class View
{
    private $baseUri = '';
    private $language = null;

    protected function language()
    {
        return $this->language;
    }

    /**
     * @param $language
     * @return self
     */
    public function setLanguage(Language $language)
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @return baseUri
     */
    public function baseUri()
    {
        return $this->baseUri;
    }

    /**
     * @param $baseUri
     * @return self
     */
    public function setBaseUri($baseUri)
    {
        $this->baseUri = $baseUri;
        return $this;
    }

    abstract protected function buildView();

    public function __tostring()
    {
        try {
            return (string)$this->buildView();
        } catch (Exception $exception) {
            if (stripos('127.0.0.1', $_SERVER['REMOTE_ADDR']) !== false && $_SERVER['SERVER_ADDR'] == '127.0.0.1') {
                var_dump($exception);
            }
            return $this->language()->translationErrorOccured();
        }
    }
}
