<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
namespace Library;

use \Library\Interfaces\ICache;
use \Library\Interfaces\ILogger;

/**
 * Store big data, which does not need to be queried or calculated every time,
 * in files to save ressources and speed up loading process.
 * To store arrays or objects, serialize them before putting them into cache.
 * @source http://www.grafikart.fr/tutoriels/php/class-cache-340
 */
class Cache implements ICache
{
    protected $logger             = null;
    protected $cacheDirectoryPath = '';
    /**
     * Cache validity period in minutes.
     * O = unlimited
     */
    protected $duration = 0;
    protected $buffer;

    /**
     * Create instance of Cache.
     * @param string $dirname Folder where to store the file containing the cache data.
     * @param int $duration Time to keep the cache data stored in minutes.
     */
    public function __construct(ILogger $logger, $cacheDirectoryPath = 'Cache', $duration = 0)
    {
        $this->logger = $logger;
        $this->cacheDirectoryPath = $cacheDirectoryPath;
        if (!$this->doesDirectoryExists()) {
            if (!mkdir($cacheDirectoryPath)) {
                $this->logger->logError(__METHOD__.':'.__LINE__.": $cacheDirectoryPath could not be created.");
            }
        }
        $this->duration = $duration;
    }

    private function cleanFilename($filename)
    {
        return filter_var($filename, FILTER_SANITIZE_STRING);
    }

    private function isFilenameValid($filename)
    {
        $isFilenameValid = true;
        if (empty($filename)) {
            $isFilenameValid = false;
            $this->logger->logError(__METHOD__.':'.__LINE__.": Filename is empty.");
        }
        return $isFilenameValid;
    }

    private function doesDirectoryExists()
    {
        return file_exists($this->cacheDirectoryPath);
    }

    public function write($filename, $content)
    {
        $isCacheWritten = false;
        if ($this->isFilenameValid(($filename = $this->cleanFilename($filename)))) {
            //@todo use FileManager
            try {
                $isCacheWritten = file_put_contents($this->cacheDirectoryPath.'/'.$filename, $content);
            } catch (Exception $exception) {
                $this->logger->logException($exception);
            }
        }
        if ($isCacheWritten) {
            $this->logger->logMessage(__METHOD__.':'.__LINE__." has written in $filename.");
        } else {
            $this->logger
                ->logError(__METHOD__.':'.__LINE__." could not write $content in $this->cacheDirectoryPath/$filename.");
        }
    }

    public function writeArray($filename, array $array)
    {
        $this->write($filename, serialize($array));
    }

    public function writeObject($filename, $object)
    {
        $this->write($filename, serialize($object));
    }

    /**
     * Return content of cache if its life span is less than duration.
     * @todo strict type, must return string.
     * I wanted to use empty string rather than false to keep strict typing,
     * but there is no difference between no cache and a cache containing an empty string.
     * @param string $filename The name of the cache file.
     * @return mixed cache content or false
     * @todo return string Content of the cache or an empty string.
     */
    public function read($filename)
    {
        $file = $this->cacheDirectoryPath.'/'.$filename;
        if (!file_exists($file)) {
            $this->logger->logMessage(__METHOD__.':'.__LINE__.": file $filename does not exist.");
            return false;
        }
        $lifetime = (time() - filemtime($file)) / 60; //time difference in minutes
        if ($this->duration > 0 && $lifetime > $this->duration) {
            $this->logger->logMessage(__METHOD__.':'.__LINE__.": file $filename expired.");
            return false;
        }
        $this->logger->logMessage(__METHOD__.':'.__LINE__.": a file named $filename");
        return file_get_contents($file);
    }

    private function readSerialized($filename, $cached)
    {
        $rawCached = $this->read($filename);
        if ($rawCached) {
            $cached = unserialize($rawCached);
        }
        return $cached;
    }

    public function readArray($filename)
    {
        return $this->readSerialized($filename, array());
    }

    public function readObject($filename)
    {
        return $this->readSerialized($filename, false);
    }

    /**
     * Delete the cache file named $filename.
     * @param string $filename The name of the cache file.
     */
    public function delete($filename)
    {
        $file = $this->cacheDirectoryPath.'/'.$filename;
        if (file_exists($file)) {
            unlink($file);
        }
    }

    private function getCacheFilesStartingWith($startPattern = '')
    {
        if (!($files = glob($this->cacheDirectoryPath.'/'.$startPattern.'*'))) {
            $files = array();
        }
        return $files;
    }

    /**
     * Clear the whole cache or only the files starting by $startPattern.
     * @param string $startPattern
     */
    public function clear($startPattern = '')
    {
        $files = $this->getCacheFilesStartingWith($startPattern);
        foreach ($files as $file) {
            unlink($file);
        }
        $this->logger->logMessage(__METHOD__.':'.__LINE__.": cleared files starting with $startPattern.");
    }

    /**
     * Write the content of $file in cache and display it.
     * If no $cachename is specified, use $file basename.
     * @param string $file The file to write to the cache.
     * @param string $cachename The name of the cache file.
     * @return bool True if $file has been read successfully.
     */
    public function inc($file, $cachename = null)
    {
        if (!$cachename) {
            $cachename = basename($file);
        }
        if ($content = $this->read($cachename)) {
            echo $content;
            return true;
        }
        // Require executes the file immediately, so we use the buffer to store it in $content.
        ob_start();
        require $file;
        $content = ob_get_clean();
        $this->write($cachename, $content);
        echo $content;
        return true;
    }

    /**
     * Start a buffer. The actions done after calling this method are written in cache.
     */
    public function start($cachename)
    {
        //If a cache already exists for $cachename, just display its content then quit.
        if ($content = $this->read($cachename)) {
            echo $content;
            $this->buffer = false;
            return true;
        }
        ob_start();
        $this->buffer = $cachename;
    }

    /**
     * Display the content of the buffer and write it in the cache.
     * @return bool|null False if no buffer.
     */
    public function end()
    {
        if (!$this->buffer) {
            return false;
        }
        $content = ob_get_clean();
        echo $content;
        $this->write($this->buffer, $content);
    }
 
    /**
     * @param $logger
     * @return self
     */
    public function setLogger(ILogger $logger)
    {
        $this->logger = $logger;
        return $this;
    }
}
