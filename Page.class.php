<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
namespace Library;

use \InvalidArgumentException;
use \RuntimeException;

/**
 * Description of Page
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
class Page extends ApplicationComponent
{
    protected $contentFile;
    protected $vars = array();
    protected $applyLayout = true;
    private $pageTitle = '';

    public function addVar($var, $value)
    {
        if (!is_string($var) || is_numeric($var) || empty($var)) {
            throw new InvalidArgumentException('The name of the variable must be a non null string.');
        }
        $this->vars[$var] = $value;
        return $this;
    }

    private function hasVar($var)
    {
        return array_key_exists($var, $this->vars);
    }

    private function setBaseUri()
    {
        if (!$this->hasVar('baseUri')) {
            $this->addVar('baseUri', '/');
        }
    }

    public function setPageTitle($pageTitle)
    {
        $this->pageTitle = $pageTitle;
    }

    public function getGeneratedPage()
    {
        $viewFileName = $this->contentFile;
        if (!file_exists($viewFileName)) {
            throw new RuntimeException("The view $viewFileName does not exist.");
        }
        $this->setBaseUri();
        extract($this->vars);
        ob_start();
            require $viewFileName;
        $content = ob_get_clean();
        if ($this->applyLayout) {
            ob_start();
                require '../Application/Templates/baseLayout.php';
            $finalRender = ob_get_clean();
        } else {
            $finalRender = $content;
        }
        return $finalRender;
    }

    public function setContentFile($contentFile)
    {
        if (!is_string($contentFile) || empty($contentFile)) {
            throw new InvalidArgumentException("The specified view $contentFile is invalid.");
        }
        $this->contentFile = $contentFile;
    }

    public function setApplyLayout($applyLayout)
    {
        $this->applyLayout = $applyLayout;
    }
}
