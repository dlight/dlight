<?php
/**
 * Copyright 2014 Didier Delhaye
 *
 * This file is part of DLight.

 * DLight is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * DLight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with DLight. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Didier Delhaye <didier.delhaye@drkdidel.be>
 */
namespace Library;

use \DateTime;
use \Exception;
use \Library\Interfaces\IConfig;
use \Library\Interfaces\ILogger;
use \Library\Interfaces\ICache;
use \Library\Interfaces\IStringUtils;

abstract class Manager
{
    private $cacheForceRefresh = false;
    private $entitiesId = array();
    private $entities = array();
    private $entity = null;
    private $errorEncountered = false;
    protected $config = null;
    protected $logger = null;
    protected $cache = null;
    protected $stringUtils = null;
    protected $searchParameters = null;
    protected $rawData = array();

    public function __construct(IConfig $config, ILogger $logger, ICache $cache, IStringUtils $stringUtils)
    {
        $this->config = $config;
        $this->logger = $logger;
        $this->cache = $cache;
        $this->stringUtils = $stringUtils;
        $this->initManager();
    }

    protected function initManager()
    {
    }

    /**
     * Manager factory.
     * Create, configure and return an instance of the manager of the entity matching $entityName.
     * By convention, all entity globalManager files must be named like
     * [entity name]Manager.class.php (e.g. UserManager.class.php).
     * Also all entity globalManager must be in the \Application\Models\ namespace.
     * @param string $entityName The name of an entity.
     * @return Manager
     */
    public function getManagerOf($entityName)
    {
        if ($entityName == 'xml') {
            $entityManagerName = '\\Library\\FileManagers\\XmlFileManager';
        } else {
            $entityManagerName = '\\Application\\Models\\'.$entityName.'Manager';
        }
        return new $entityManagerName($this->config, $this->logger, $this->cache, $this->stringUtils);
    }

    public function getCache($filename)
    {
        $cacheContent = array();
        if (!$this->cacheForceRefresh) {
            if (($rawCache = $this->cache->read($filename)) !== false) {
                $cacheContent = unserialize($rawCache);
            }
        }
        return $cacheContent;
    }

    protected function cacheEntity($filename, $entity)
    {
        $this->cache->write($filename, serialize($entity));
    }

    private function doesDataExist(array $data, $key)
    {
        return array_key_exists($key, $data);
    }

    /**
     * Returns an array of integer IDs from $objects.
     *
     * @param array $objects The objects from which to fetch IDs.
     * @param string $attributeName The name of the attribute
     * representing the ID in $objects.
     */
    protected function fetchIDs(array $objects, $attributeName)
    {
        $ids = array();
        foreach ($objects as $object) {
            $ids[] = (int)$object[$attributeName];
        }
        return $ids;
    }

    private function removeAliasFromAttributeName(&$attributeName)
    {
        $tupleAlias = '';
        if (($attributeNamePosition = strrpos($attributeName, '.')) !== false) {
            // Must be executed before $attributeName is edited.
            $tupleAlias = substr($attributeName, 0, $attributeNamePosition);
            $attributeName = substr($attributeName, $attributeNamePosition + 1);
        }
        return $tupleAlias;
    }

    protected function castEntitiesByGroup(
        $attributeNameAsKeyForGroup,
        $attributeNameAsKeyLevelTwo,
        &$entitiesParent,
        $setterMethodName
    ) {
        $rawDataMultiple = $this->rawDataMultiple;
        $this->rawDataMultiple = array();
        $key = '';
        $keyPrevious = '';
        $isFirstLoop = true;
        foreach ($rawDataMultiple as $rawData) {
            $key = $rawData[$this->tupleAlias().".$attributeNameAsKeyForGroup"];
            if (!$isFirstLoop && $key !== $keyPrevious) {
                $this->castEntities($attributeNameAsKeyLevelTwo);
                $this->rawDataMultiple = array();
                $entitiesParent[$keyPrevious]->$setterMethodName($this->entities);
                $this->entities = array();
            }
            $this->rawDataMultiple[] = $rawData;
            $keyPrevious = $key;
            $isFirstLoop = false;
        }
        if (!$isFirstLoop) {
            $this->castEntities($attributeNameAsKeyLevelTwo);
            $entitiesParent[$keyPrevious]->$setterMethodName($this->entities);
        }
    }

    // Override this if we need to generate different entities according to some conditions.
    protected function generateEntity()
    {
    }

    protected function castEntities($attributeNameAsKey = 'id')
    {
        $emptyEntity = clone $this->entity; // Save the new empty entity as a model.
        foreach ($this->rawDataMultiple as $rawData) {
            $this->rawData = $rawData;
            $this->generateEntity();
            $this->castEntity();
            if (!$this->entity->$attributeNameAsKey()) {
                $this->logger->logMessage(sprintf(
                    "%s:%d: %s::$attributeNameAsKey is maybe not set (value: %s)",
                    __METHOD__,
                    __LINE__,
                    get_class($this->entity),
                    $this->entity->$attributeNameAsKey()
                ));
            }
            // Save a copy of the updated entity.
            $this->entities[$this->entity->$attributeNameAsKey()] = clone $this->entity;
            $this->entitiesId[] = $this->entity->$attributeNameAsKey();
            $this->entity = clone $emptyEntity; // Reset the entity.
        }
    }

    /**
     * Hydrate self::entity with self::rawData.
     * Call self::castAttribute on each row of self::rawData.
     * If the result is not null, hydrate self::entity.
     * @todo should browse the entity for attribute names and only process matching raw data,
     * no matter the source, like database or form (see FormHandler)
     */
    public function castEntity()
    {
        //var_dump($this->rawData);die;
        foreach ($this->rawData as $attributeName => $attributeValue) {
            //@todo if rawData contains multiple aliases,
            //compare alias returned by removeAliasFromAttributeName with current entity class name.
            $alias = $this->removeAliasFromAttributeName($attributeName);
            $castedAttribute = $this->castAttribute($attributeName, $attributeValue);
            if ($castedAttribute !== null) {
                $setterMethodName = 'set'.ucfirst($attributeName);
                $this->entity->$setterMethodName($castedAttribute);
            }
        }
    }

    /**
     * Cast a single attribute.
     * @param string $attributeName belonging to $this->entity
     * @param string $attributeValue raw value to cast according to the type of entity::$attributeName
     */
    private function castAttribute($attributeName, $attributeValue)
    {
        switch(gettype($this->entity->$attributeName()))
        {
            case 'integer':
                $attributeValue = (int)$attributeValue;
                break;
            case 'double':
                $attributeValue = (double)$attributeValue;
                break;
            case 'boolean':
                $attributeValue = (bool)$attributeValue;
                break;
            case 'object':
                switch($className = $this->stringUtils->className($this->entity->$attributeName()))
                {
                    case 'DateTime':
                        if (($date = DateTime::createFromFormat('Y-m-d H:i:s', $attributeValue)) !== false) {
                            $attributeValue = $date;
                        } else {
                            $attributeValue = null;
                        }
                        break;
                }
                break;
            default:
                $attributeValue = (string)$attributeValue;
                break;
        }
        return $attributeValue;
    }

    public function setSearchParameters(Entity $searchParameters)
    {
        $this->searchParameters = $searchParameters;
        return $this;
    }

    private function searchParameters()
    {
        return $this->searchParameters;
    }

    private function areSearchParametersSet()
    {
        return $this->searchParameters() !== null;
    }

    protected function searchParameter($parameterName, $methodParameter = null)
    {
        $parameterValue = null;
        if ($this->areSearchParametersSet() && method_exists($this->searchParameters(), $parameterName)) {
            $parameterValue = $this->searchParameters()->$parameterName($methodParameter);
        }
        return $parameterValue;
    }

    /**
     * @return entitiesId
     */
    public function entitiesId()
    {
        return $this->entitiesId;
    }

    protected function hasAttachment($attributeName, $childNumber = 0, $childAttributeName = '')
    {
        $hasAttachment = false;
        //var_dump($_FILES);die;
        if (!empty($childAttributeName)) {
            //$childNumber is not set after deleting an offer
            $hasAttachment = isset($_FILES[$attributeName]['tmp_name'][$childNumber]) &&
                $_FILES[$attributeName]['tmp_name'][$childNumber][$childAttributeName] != '';
        } else {
            $hasAttachment = $_FILES[$attributeName]['tmp_name'] != '';
        }
        return $hasAttachment;
    }

    private function uploadedFileAttribute($attributeName, $fileAttribute, $childNumber = 0, $childAttributeName = '')
    {
        $filename = '';
        if (isset($_FILES[$attributeName][$fileAttribute])) {
            if (!empty($childAttributeName) &&
                isset($_FILES[$attributeName][$fileAttribute][$childNumber][$childAttributeName])
           ) {
                $filename = $_FILES[$attributeName][$fileAttribute][$childNumber][$childAttributeName];
            } else {
                $filename = $_FILES[$attributeName][$fileAttribute];
            }
        }
        $this->logger->logMessage(__METHOD__.':'.__LINE__.": filename is $filename.");
        return $filename;
    }

    /**
     * Move the uploaded file in its final destination.
     * @param string $attributeName
     * @param string $prefix Use this to create a unique filename to avoid it being overwritten.
     * @return string The final file name if successful or an empty string.
     */
    protected function addFileAttachment($attributeName, $prefix = '', $childNumber = 0, $childAttributeName = '')
    {
        //var_dump($_FILES);
        //var_dump(array('attributeName' => $attributeName, 'childAttributeName' => $childAttributeName));
        $savedName = '';
        $finalPath = '';
        try {
            $uploadedFilename = $this->uploadedFileAttribute(
                $attributeName,
                'name',
                $childNumber,
                $childAttributeName
            );
            $uploadedFileTmpName = $this->uploadedFileAttribute(
                $attributeName,
                'tmp_name',
                $childNumber,
                $childAttributeName
            );
            if (!empty($uploadedFilename) && !empty($uploadedFileTmpName)) {
                $finalName = $prefix.$uploadedFilename;
                $configAttributeName = $attributeName;
                if (!empty($childAttributeName)) {
                    $configAttributeName .= ucfirst($childAttributeName);
                }
                $configAttributeName .= 'UploadPath';
                //var_dump($configAttributeName);
                $finalPath = $this->config->getString($configAttributeName).$finalName;
                if (move_uploaded_file($uploadedFileTmpName, $finalPath)) {
                    $savedName = $finalName;
                    $this->logger->logMessage(__METHOD__.':'.__LINE__.": saved $uploadedFileTmpName to $finalPath.");
                } else {
                    $this->logger->logError(
                        __METHOD__.':'.__LINE__.
                        ": could not move $uploadedFileTmpName to $finalPath."
                    );
                }
            }
        } catch (Exception $exception) {
            $this->logger->logError(__METHOD__.':'.__LINE__.': Attachment could not be saved.');
            $this->logger->logException($exception);
        }
        return $savedName;
    }

    private function checkEntity()
    {
        if (!$this->entity) {
            throw new Exception('Entity is not set.');
        }
    }

    public function entity()
    {
        try {
            $this->checkEntity();
        } catch (Exception $exception) {
            $this->logger->logException($exception);
        }
        return $this->entity;
    }

    public function setEntity(Entity $entity)
    {
        $this->entity = $entity;
        return $this;
    }

    protected function castAndStoreEntity()
    {
        $this->castEntity();
        $this->entities[] = clone $this->entity;
        return $this;
    }

    public function entities()
    {
        return $this->entities;
    }

    public function setRawData(array $rawData)
    {
        $this->rawData = $rawData;
        return $this;
    }

    public function rawData()
    {
        return $this->rawData;
    }

    /**
     * @return rawDataString
     */
    public function rawDataString()
    {
        return $this->rawDataString;
    }

    protected function rawDataInt()
    {
        return (int)$this->rawDataString();
    }

    protected function rawDataBool()
    {
        return (bool)$this->rawDataString();
    }

    /**
     * @return rawDataMultiple
     */
    public function rawDataMultiple()
    {
        return $this->rawDataMultiple;
    }

    /**
     * @param $cacheForceRefresh
     * @return self
     */
    public function setCacheForceRefresh()
    {
        $this->cacheForceRefresh = true;
        return $this;
    }

    public function errorEncountered()
    {
        return $this->errorEncountered;
    }

    /**
     * Set self::errorEncountered if it is not already true.
     * @param bool $errorEncountered
     * @return self
     */
    public function setErrorEncountered($errorEncountered)
    {
        if (!$this->errorEncountered) {
            $this->errorEncountered = (bool)$errorEncountered;
        }
        return $this;
    }
}
